//
//  GameViewController.swift
//  physicsTest
//
//  Created by Francis Conlon on 11/02/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import UIKit
import SpriteKit
import GameplayKit
import GameKit
import PersonalizedAdConsent
import GoogleMobileAds

class GameViewController: UIViewController, GKGameCenterControllerDelegate {

    

    var saveData: SaveDataSource!
    var gcEnabled = Bool()
    var gcDefaultLeaderboard = String()
    let LEADERBOARD_ID = "com.overvoltstudios.towerclimber.highscore"
    var gameScene: GameScene!
    var interstitial: GADInterstitial!
    
    var interstitialID = "ca-app-pub-8001969410865481/5664900589"
    var rewardID = "ca-app-pub-8001969410865481/2596470844"
    var applicationID = "ca-app-pub-8001969410865481~5090061805"
    
    var heightLeaderboardID = "com.overvoltstudios.towerclimber.highestheightclimbed"
    var coinsLeaderboardID = "com.overvoltstudios.towerclimber.mostcoinscollected"
    var enemiesLeaderboardID = "com.overvoltstudios.towerclimber.mostenemieshit"
    
    var advertHolds = 0

    
    override func viewDidLoad()
    {
        super.viewDidLoad()
        
        if #available(iOS 11.0, *) {
        } else {
            // Fallback on earlier versions
        }
        
        interstitial = GADInterstitial(adUnitID: interstitialID)
        interstitial.delegate = self
        
        PACConsentInformation.sharedInstance.requestConsentInfoUpdate(forPublisherIdentifiers: ["pub-8001969410865481"])
        {(_ error: Error?) -> Void in
            if let error = error {
                // Consent info update failed.
            } else {
                // Consent info update succeeded. The shared PACConsentInformation
                // instance has been updated.
                self.configureAds()
            }
        }
    
        
        
        
        
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(willResignActive(_:)),
                                               name: Notification.Name("willResignActive"),
                                               object: nil)
        // Load or create game data
        saveData = SaveDataSource()
        
        if saveData.backgroundMusic
        {
            SKTAudio.sharedInstance().playBackgroundMusic("Rhinoceros.mp3")
            SKTAudio.sharedInstance().backgroundMusicPlayer!.volume = 0.5
        }
        
        // Load 'GameScene.sks' as a GKScene. This provides gameplay related content
        // including entities and graphs.
        if let scene = GKScene(fileNamed: "GameScene") {
            
            // Get the SKScene from the loaded GKScene
            if let sceneNode = scene.rootNode as! GameScene? {
                
                // Copy gameplay related content over to the scene
  
                
                // Set the scale mode to scale to fit the window
                sceneNode.scaleMode = .aspectFill
                sceneNode.saveDataSource = saveData
                gameScene = sceneNode
                // Present the scene
                if let view = self.view as! SKView? {
                    view.presentScene(sceneNode)
                    
                    view.ignoresSiblingOrder = true
                    
                    //view.showsFPS = true
                    //view.showsNodeCount = true
                    //view.showsPhysics = true
                    
                
                }
            }
        }
    }


    
    
    override var shouldAutorotate: Bool {
        return true
    }

    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        if UIDevice.current.userInterfaceIdiom == .phone {
            return .allButUpsideDown
        } else {
            return .all
        }
    }

    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    @objc func willResignActive(_ notification: Notification)
    {
        saveData.saveChanges()
    }
    
    func authenticateLocalPlayer()
    {
        
        let localPlayer: GKLocalPlayer = GKLocalPlayer.local
        
        localPlayer.authenticateHandler = {(ViewController, error) -> Void in
            if((ViewController) != nil) {
                // 1. Show login if player is not logged in
                self.present(ViewController!, animated: true, completion: nil)
            } else if (localPlayer.isAuthenticated) {
                // 2. Player is already authenticated & logged in, load game center
                self.gcEnabled = true
                
                // Get the default leaderboard ID
                localPlayer.loadDefaultLeaderboardIdentifier(completionHandler: { (leaderboardIdentifer, error) in
                    if error != nil { print(error)
                    } else { self.gcDefaultLeaderboard = leaderboardIdentifer! }
                })
                
                
            } else {
                // 3. Game center is not enabled on the users device
                self.gcEnabled = false
            }
            self.gameScene.hud.gameCenterNode.isUserInteractionEnabled = true
        }
    }
    
    func authenticatePlayerAndSubmitHighScores(height: Int, coins: Int, enemies: Int)
    {
        
        let localPlayer: GKLocalPlayer = GKLocalPlayer.local
        
        localPlayer.authenticateHandler = {(ViewController, error) -> Void in
            if((ViewController) != nil) {
                // 1. Show login if player is not logged in
                self.present(ViewController!, animated: true, completion: {self.submitHighScores(height: height, coins: coins, enemies: enemies)})
            } else if (localPlayer.isAuthenticated) {
                // 2. Player is already authenticated & logged in, load game center
                self.gcEnabled = true
                self.submitHighScores(height: height, coins: coins, enemies: enemies)
                // Get the default leaderboard ID
                localPlayer.loadDefaultLeaderboardIdentifier(completionHandler: { (leaderboardIdentifer, error) in
                    if error != nil {
                    } else { self.gcDefaultLeaderboard = leaderboardIdentifer! }
                })
                
                
            } else {
                // 3. Game center is not enabled on the users device
                self.gcEnabled = false
            }
        }
    }
    
    func submitHighScores(height: Int, coins: Int, enemies: Int)
    {
        
        let bestHeightInt = GKScore(leaderboardIdentifier: heightLeaderboardID)
        bestHeightInt.value = Int64(height)
        let bestCoinsInt = GKScore(leaderboardIdentifier: coinsLeaderboardID)
        bestCoinsInt.value = Int64(coins)
        let bestEnemiesInt = GKScore(leaderboardIdentifier: enemiesLeaderboardID)
        bestEnemiesInt.value = Int64(enemies)
        
        if gcEnabled {
            GKScore.report([bestHeightInt, bestCoinsInt, bestEnemiesInt], withCompletionHandler: { (error) in
                if error != nil {
                } else {
                    let gcVC = GKGameCenterViewController()
                    gcVC.gameCenterDelegate = self
                    gcVC.viewState = .leaderboards
                    //gcVC.leaderboardIdentifier = self.heightLeaderboardID
                    self.present(gcVC, animated: true, completion: nil)
                }})
        } else {
            authenticatePlayerAndSubmitHighScores(height: height, coins: coins, enemies: enemies)
        }

        
        
        //authenticateLocalPlayer()
    }
    
    func presentGameCenter()
    {
        let gcVC = GKGameCenterViewController()
        gcVC.gameCenterDelegate = self
        gcVC.viewState = .leaderboards
        self.present(gcVC, animated: true, completion: nil)
    }
    
    func gameCenterViewControllerDidFinish(_ gameCenterViewController: GKGameCenterViewController) {
        gameCenterViewController.dismiss(animated: true, completion: {self.gameScene.hud.gameCenterNode.isUserInteractionEnabled = true})
    }
    
    func npaRequest() -> GADRequest
    {
        let request = GADRequest()
        let extras = GADExtras()
        extras.additionalParameters = ["npa" : "1"]
        request.register(extras)
        return request
    }
    
    func configureAds()
    {
        
        
        if !PACConsentInformation.sharedInstance.isRequestLocationInEEAOrUnknown
        {
         
                GADMobileAds.configure(withApplicationID: self.applicationID)
                GADRewardBasedVideoAd.sharedInstance().load(GADRequest(),
                                                            withAdUnitID: self.rewardID)
            
        } else if PACConsentInformation.sharedInstance.consentStatus == .nonPersonalized {
            
                GADMobileAds.configure(withApplicationID: self.applicationID)
                let npaReq = self.npaRequest()
                GADRewardBasedVideoAd.sharedInstance().load(npaReq,
                                                            withAdUnitID: self.rewardID)
                let intnpaReq = self.npaRequest()
                self.interstitial.load(intnpaReq)
            
            
        } else if PACConsentInformation.sharedInstance.consentStatus == .personalized {
            
            
                GADMobileAds.configure(withApplicationID: self.applicationID)
                GADRewardBasedVideoAd.sharedInstance().load(GADRequest(),
                                                            withAdUnitID: self.rewardID)
                
                self.interstitial.load(GADRequest())
            
        } else if PACConsentInformation.sharedInstance.consentStatus == .unknown {
            //PACConsentInformation.sharedInstance.consentStatus = .personalized
            let consentVC = ConsentViewController()
            consentVC.modalPresentationStyle = .overFullScreen
            self.present(consentVC, animated: true, completion: {})
        }
        GADRewardBasedVideoAd.sharedInstance().delegate = self
    }
    
    func presentRewardedAd()
    {
        GADRewardBasedVideoAd.sharedInstance().present(fromRootViewController: self)
        SKTAudio.sharedInstance().pauseBackgroundMusic()
    }
}

extension GameViewController: GADRewardBasedVideoAdDelegate
{
    func rewardBasedVideoAd(_ rewardBasedVideoAd: GADRewardBasedVideoAd, didRewardUserWith reward: GADAdReward)
    {
        saveData.coins += 100
        saveData.saveChanges()
        
    }
    
    func rewardBasedVideoAdDidReceive(_ rewardBasedVideoAd: GADRewardBasedVideoAd)
    {
    }
    
    func rewardBasedVideoAdDidClose(_ rewardBasedVideoAd: GADRewardBasedVideoAd)
    {
        let request = GADRequest()
        
        if PACConsentInformation.sharedInstance.consentStatus == .nonPersonalized {
            let extras = GADExtras()
            extras.additionalParameters = ["npa" : "1"]
            request.register(extras)
        }
        
        GADRewardBasedVideoAd.sharedInstance().load(request,
                                                    withAdUnitID: rewardID)
        
        if saveData.backgroundMusic { SKTAudio.sharedInstance().resumeBackgroundMusic() }
        
        //gameScene.hud.coinsNode.updateCoinsLabel()
        gameScene.afterDelay(0.5, runBlock: {self.gameScene.hud.coinsNode.updateCoinsLabel()})
        SKTAudio.sharedInstance().playSoundEffect("coin.wav")
    }
    

}



extension GameViewController: GADInterstitialDelegate
{
    func interstitialDidReceiveAd(_ ad: GADInterstitial) {
    }
    
    func presentInterstitial()
    {
        if interstitial.isReady
        {
            //interstitial.present(fromRootViewController: self)
        }
    }
    
    func interstitialDidDismissScreen(_ ad: GADInterstitial)
    {
     
    }
    
    func reloadInterstitial()
    {
        if PACConsentInformation.sharedInstance.consentStatus == .nonPersonalized {
            let req = npaRequest()
            interstitial = GADInterstitial(adUnitID: interstitialID)
            interstitial.delegate = self
            interstitial.load(req)
        } else if PACConsentInformation.sharedInstance.consentStatus == .personalized {
            let req = GADRequest()
            interstitial = GADInterstitial(adUnitID: interstitialID)
            interstitial.delegate = self
            interstitial.load(req)
        }
    }
}
