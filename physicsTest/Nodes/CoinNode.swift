//
//  HoldNode.swift
//  physicsTest
//
//  Created by Francis Conlon on 12/02/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import SpriteKit

class CoinNode: SKNode, InteractiveNode, EventListenerNode
{
    var gameScene: GameScene!
    var used = false
    
    var coinNode: SKSpriteNode!
    
    func interact()
    {
        gameScene = scene! as? GameScene
        
        
        let nodePosition = self.parent!.convert(self.position, to: scene!)
        
        let rightDistance = CGVector(point: nodePosition - gameScene.person.rightHandPhysicsNode.position).length()
        let leftDistance = CGVector(point: nodePosition - gameScene.person.leftHandPhysicsNode.position).length()
        
        if leftDistance < gameScene.reach || rightDistance < gameScene.reach
        {
            self.isPaused = false
            if leftDistance < rightDistance
            {
                gameScene.person.rightHandPhysicsNode.run(SKAction.sequence([ SKAction.move(to: nodePosition, duration: 0.2),
                                                                              SKAction.run {self.gameScene.person.rightHandPhysicsNode.physicsBody!.isDynamic = true}]))
                self.afterDelay(0.2, runBlock: {SKAction.sequence([SKAction.scale(to: 0, duration: 0.2), SKAction.run { self.gameScene.coins += 1 }])})
                gameScene.person.torso.physicsBody?.applyImpulse(CGVector(dx: 0, dy: 150))
            } else {
                gameScene.person.leftHandPhysicsNode.run(SKAction.sequence([SKAction.move(to: nodePosition, duration: 0.2),
                                                                            SKAction.run{self.gameScene.person.leftHandPhysicsNode.physicsBody!.isDynamic = true}]))
                self.afterDelay(0.2, runBlock: {SKAction.sequence([SKAction.scale(to: 0, duration: 0.2), SKAction.run {  }])})
                gameScene.person.torso.physicsBody?.applyImpulse(CGVector(dx: 0, dy: 150))
            }
            let emitterNode = SKEmitterNode(fileNamed: "CoinParticle.sks")!
            emitterNode.targetNode = gameScene
            emitterNode.zPosition = 20
            emitterNode.position = self.parent!.convert(self.position, to: gameScene)
            let sound = SKAction.playSoundFileNamed("coin.wav", waitForCompletion: false)
            coinNode.run(SKAction.sequence([SKAction.wait(forDuration: 0.2),
                                            SKAction.run { self.gameScene.run(sound) },
                                            SKAction.scale(to: 0, duration: 0.2),
                                            SKAction.run {
                                                self.gameScene.coins += 1
                                                self.gameScene.addChild(emitterNode)

                },
                                            SKAction.removeFromParent()]))
            holdTapped()
            used = true
            
        }
    }
    
    
    func didMoveToScene()
    {
        
        
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        
        interact()
    }
    
    
    override init() {
        super.init()
        self.name = "CoinNode"
        self.zPosition = 10
        let textureAtlas = SKTextureAtlas(named: "coins")
        var rotateFrames: [SKTexture] = []
        let numImages = textureAtlas.textureNames.count
        for i in 1...numImages
        {
            let coinName = "coin\(i)"
            rotateFrames.append(textureAtlas.textureNamed(coinName))
        }
        let firstFrameTexture = rotateFrames[0]
        coinNode = SKSpriteNode(texture: firstFrameTexture)
        coinNode.setScale(0.65)
        coinNode.position = CGPoint.zero
        addChild(coinNode)
        
        coinNode.run(SKAction.sequence([SKAction.wait(forDuration: 0.1, withRange: 0.05),
            SKAction.repeatForever(SKAction.animate(with: rotateFrames, timePerFrame: 0.1, resize: false, restore: false))]))
        coinNode.color = UIColor.red
        self.isUserInteractionEnabled = true
    }
    
    func setupCoin()
    {
        

        
        isUserInteractionEnabled = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    func holdTapped()
    {
        isUserInteractionEnabled = false
    }
    
}
