//
//  HoldNode.swift
//  physicsTest
//
//  Created by Francis Conlon on 12/02/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import SpriteKit

class HoldNode: SKNode, InteractiveNode, EventListenerNode
{
    var gameScene: GameScene!
    var used = false
    var coinVector: CGVector!
    
    var captureNode: SKShapeNode!
    var indicatorNode: SKShapeNode!
    
    func interact()
    {
        
        gameScene = scene! as! GameScene
        
        if gameScene.gameState.currentState is Falling { return }
        
        let nodePosition = self.parent!.convert(self.position, to: scene!)
        let torsoPosition = gameScene.person.convert(gameScene.person.torso.position, to: scene!)
        
        let rightDistance = CGVector(point: nodePosition - gameScene.person.rightHandPhysicsNode.position).length()
        let leftDistance = CGVector(point: nodePosition - gameScene.person.leftHandPhysicsNode.position).length()
        
        if leftDistance < gameScene.reach || rightDistance < gameScene.reach
        {
            if nodePosition.x > torsoPosition.x {
                let midpoint = (nodePosition + gameScene.person.rightHandPhysicsNode.position) / 2
                let vector = CGVector(point: midpoint - gameScene.person.convert(gameScene.person.torso.position, to: scene!))
                gameScene.person.rightHandPhysicsNode.physicsBody!.isDynamic = false
                gameScene.person.rightHandPhysicsNode.run(SKAction.move(to: nodePosition, duration: 0.2), withKey: "handmove")
                gameScene.person.rightHandPhysicsNode.run(SKAction.move(to: nodePosition, duration: 0.2))
            
                //rightHandPhysicsNode.run(SKAction.move(to: background.convert(node.parent!.position, to: scene!), duration: 0.2))
                gameScene.person.torso.physicsBody?.applyImpulse(vector * 1)
                
                gameScene.person.leftHandPhysicsNode.run(SKAction.sequence([SKAction.wait(forDuration: 0.2),
                                                                            SKAction.run {self.gameScene.person.leftHandPhysicsNode.physicsBody!.isDynamic = false},
                                                                            SKAction.move(to: nodePosition, duration: 0.2)]), withKey: "handmove")
            } else {
                let midpoint = (nodePosition + gameScene.person.leftHandPhysicsNode.position) / 2
                let vector = CGVector(point: midpoint - gameScene.person.convert(gameScene.person.torso.position, to: scene!))
                gameScene.person.leftHandPhysicsNode.physicsBody!.isDynamic = false
                gameScene.person.leftHandPhysicsNode.run(SKAction.move(to: nodePosition, duration: 0.2), withKey: "handmove")
                //rightHandPhysicsNode.run(SKAction.move(to: background.convert(node.parent!.position, to: scene!), duration: 0.2))
                gameScene.person.torso.physicsBody?.applyImpulse(vector * 1)
                
                gameScene.person.rightHandPhysicsNode.run(SKAction.sequence([SKAction.wait(forDuration: 0.2),
                                                                            SKAction.run {self.gameScene.person.rightHandPhysicsNode.physicsBody!.isDynamic = false},
                                                                            SKAction.move(to: nodePosition, duration: 0.2)]), withKey: "handmove")
            }
            
            
            
            
            
            
            holdTapped()
            used = true
            
            if gameScene.gameState.currentState is Climbing
            {
                gameScene.score += 1
            }
            
            if gameScene.comboPossible && gameScene.comboIndex < 2
            {
                gameScene.comboIndex += 1
                gameScene.removeAction(forKey: "combo")
            } else if gameScene.comboPossible == false {
                gameScene.comboIndex = 1
                gameScene.comboPossible = true
            }
            gameScene.run(SKAction.playSoundFileNamed("combo\(gameScene.comboIndex).m4a", waitForCompletion: false), withKey: "comboAudio")
            gameScene.run(SKAction.sequence([SKAction.wait(forDuration: 0.7),
                                             SKAction.run {
                                                self.gameScene.comboPossible = false
                }]), withKey: "combo")
        }
    }
    
    
    func didMoveToScene()
    {
        
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        
        interact()
    }
    
    override init()
    {
        
        super.init()
        self.name = "HoldNode"
    }
    
    func setupHold()
    {
        captureNode = SKShapeNode(circleOfRadius: 100)
        captureNode.strokeColor = SKColor.clear
        captureNode.lineWidth = 5
        captureNode.zPosition = CGFloat(zPositions.handholds)
        addChild(captureNode)
        //shapeNode.physicsBody = SKPhysicsBody(circleOfRadius: 50)
        
        indicatorNode = SKShapeNode(circleOfRadius: 100)
        indicatorNode.strokeColor = SKColor.green
        indicatorNode.lineWidth = 10
        indicatorNode.zPosition = CGFloat(zPositions.handholds)
        addChild(indicatorNode)
        
        let shrink = SKAction.scale(to: 0.8, duration: 0.8)
        shrink.timingMode = .easeInEaseOut
        let grow = SKAction.scale(to: 1, duration: 0.8)
        grow.timingMode = .easeInEaseOut
        
        let sequence = SKAction.sequence([shrink, SKAction.wait(forDuration: 0.2), grow])
        indicatorNode.run(SKAction.repeatForever(sequence), withKey: "idle")
        
        let middleNode = SKShapeNode(circleOfRadius: 5)
        middleNode.strokeColor = SKColor.blue
        middleNode.fillColor = SKColor.blue
        middleNode.zPosition = CGFloat(zPositions.handholds)
        addChild(middleNode)
        
        isUserInteractionEnabled = true
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    func holdTapped()
    {
        let dissapear = SKAction.scale(to: 0, duration: 0.3)
        dissapear.timingMode = .easeIn
        captureNode.run(dissapear)
        indicatorNode.removeAction(forKey: "idle")
        indicatorNode.run(dissapear)
        isUserInteractionEnabled = false
        gameScene.currentHold = self
    }
    
}
