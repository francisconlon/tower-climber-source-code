//
//  BackgroundNode.swift
//  physicsTest
//
//  Created by Francis Conlon on 13/02/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import SpriteKit

class BackgroundNode: SKSpriteNode
{
    var holdNodes = [HoldNode]()
    var maxReach: CGFloat!
    var lastHoldOfPrevious: HoldNode!
    var lastHoldOfCurrent: HoldNode!
    
    var pathStart: CGPoint
    var pathEnd: CGPoint!
    
    func setupBackground()
    {
        
    }
    
    func initialBackgroundSetup()
    {
        for i in 0..<holdNodes.count
        {
            let hold = holdNodes[i]
            hold.setScale(0)
            hold.run(SKAction.scale(to: 1, duration: 0.5))
        }
    }
    
    init(size: CGSize, pathStart: CGPoint, reach: CGFloat, count: Int)
    {
        var noOfCoins = 0
        
        if count > 0 && count < 3
        {
            noOfCoins = 1
        } else if count > 2 && count < 5 {
            noOfCoins = 2
        } else if count > 4 {
            noOfCoins = 3
        }
        
        self.pathStart = pathStart
        
        super.init(texture: nil, color: UIColor.clear, size: size)
        
        self.anchorPoint = CGPoint(x: 0.5, y: 0)
        
        self.pathEnd = CGPoint(x: CGFloat.random(min: -size.width/2, max: size.width/2),
                               y: size.height)
        
        
        let backgroundIndex = count % 3
        let background = SKSpriteNode(imageNamed: "background\(backgroundIndex).png")
        
        background.alpha = 1
        background.zPosition = CGFloat(zPositions.tower)
        background.position = CGPoint(x: 0, y: size.height / 2)
        addChild(background)
        

        
        var last = pathStart
        //y += 150
        while (pathEnd - last).length() > reach
        {

     
            var newVector = CGVector()
            if holdNodes.last != nil {
                newVector = CGVector(dx: pathEnd.x - holdNodes.last!.position.x,
                                     dy: pathEnd.y - holdNodes.last!.position.y)
            } else {
                newVector = CGVector(dx: (pathEnd.x - pathStart.x), dy: (pathEnd.y - pathStart.y))
            }
            
            let positionVector = newVector.normalized() * 200
            var randomVariancedVector = CGVector(angle: (positionVector.angle + (CGFloat.random(min: -45, max: 45)).degreesToRadians()))
            randomVariancedVector = randomVariancedVector * CGFloat.random(min: 200, max: reach - 1)
            let hold = HoldNode()
            hold.setupHold()
            
            var angle = CGFloat()
            if Int.random(min: 0, max: 1) == 1
            {
                angle = CGFloat.random(min: 80, max: 100)
            } else {
                angle = -(CGFloat.random(min: 80, max: 100))
            }
            
            hold.coinVector = CGVector(angle: (randomVariancedVector.angle + angle.degreesToRadians()))
            
            
            
            var newPosition = CGPoint()
            if holdNodes.last != nil {
                newPosition = holdNodes.last!.position + randomVariancedVector
            } else {
                newPosition = pathStart + randomVariancedVector
            }
            
            if newPosition.x < -size.width/2
            {
                newPosition.x = -size.width/2
            } else if newPosition.x > size.width/2 {
                newPosition.x = size.width/2
            }
            hold.position = newPosition
            


            
            holdNodes.append(hold)

            addChild(hold)
            hold.isUserInteractionEnabled = true
            hold.isPaused = false
            last = hold.position
        }
        
        var moneyNodes = [HoldNode]()
        switch noOfCoins {
        case 1:
            let coinOne = holdNodes[Int(holdNodes.count/2 - 1)]
            moneyNodes = [coinOne]
        case 2:
            let coinOne = holdNodes[Int((holdNodes.count/3) - 1)]
            let coinTwo = holdNodes[Int(((holdNodes.count/3) * 2) - 1)]
            moneyNodes = [coinOne, coinTwo]
        case 3:
            let coinOne = holdNodes[Int(((holdNodes.count/4)) - 1)]
            let coinTwo = holdNodes[Int(((holdNodes.count/4) * 2) - 1)]
            let coinThree = holdNodes[Int(((holdNodes.count/4) * 3) - 1)]
            moneyNodes = [coinOne, coinTwo, coinThree]
        default:
            break
        }
        
        for node in moneyNodes
        {
            let coinPosition = node.position + node.coinVector.normalized() * CGFloat.random(min: 120, max: reach - 10)
            let coin = CoinNode()
            addChild(coin)
            coin.position = coinPosition
        }
        
        lastHoldOfCurrent = holdNodes.last!
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
}
