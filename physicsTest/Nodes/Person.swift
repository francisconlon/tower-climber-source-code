//
//  Person.swift
//  physicsTest
//
//  Created by Francis Conlon on 11/02/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import SpriteKit

class Person: SKNode
{
    
    var arms: SKNode!
    var armChain: SKNode!
    
    var joints: [SKPhysicsJoint]!
    var parts: [SKNode]!
    
    var shoulders: SKNode!
    var hips: SKNode!
    
    var background: SKNode!
    
    var torso: SKNode!
    var head: SKNode!
    
    var leftArmUpper: SKNode!
    var leftArmLower: SKNode!
    var leftHand: SKNode!
    var leftHandPhysicsNode: SKShapeNode!
    
    var rightArmUpper: SKNode!
    var rightArmLower: SKNode!
    var rightHand: SKNode!
    var rightHandPhysicsNode: SKShapeNode!
    
    var rightLegUpper: SKNode!
    var rightLegLower: SKNode!
    var rightFoot: SKNode!
    
    var leftLegUpper: SKNode!
    var leftLegLower: SKNode!
    var leftFoot: SKNode!
    
    var shirtMain: SKSpriteNode!
    var shirtShoulders: SKSpriteNode!
    var shirtRightArm: SKSpriteNode!
    var shirtLeftArm: SKSpriteNode!
    var shirtRightArmLower: SKSpriteNode!
    var shirtLeftArmLower: SKSpriteNode!
    
    var legsHips: SKSpriteNode!
    var legsRightLeg: SKSpriteNode!
    var legsLeftLeg: SKSpriteNode!
    var legsLeftLegLower: SKSpriteNode!
    var legsRightLegLower: SKSpriteNode!
    
    var hair: SKSpriteNode!
    
    var reach: CGFloat!
    
    var shirtTextures: [SKTexture]! {
        didSet {
            var smTexts = [SKTexture]()
            var ssTexts = [SKTexture]()
            var sraTexts = [SKTexture]()
            var slaTexts = [SKTexture]()
            var sralTexts = [SKTexture]()
            var slalTexts = [SKTexture]()
            
            for texture in shirtTextures
            {
                let smText = SKTexture(rect: CGRect(origin: texture.textureRect().origin,
                                                    size: CGSize(width: shirtMain.frame.size.width/texture.size().width,
                                                                 height: shirtMain.frame.size.height/texture.size().height)),
                                                    in: texture)
                smTexts.append(smText)
                let ssText = SKTexture(rect: CGRect(origin: texture.textureRect().origin,
                                                    size: CGSize(width: shirtShoulders.frame.size.width/texture.size().width,
                                                                 height: shirtShoulders.frame.size.height/texture.size().height)),
                                                    in: texture)
                ssTexts.append(ssText)
                let sraText = SKTexture(rect: CGRect(origin: texture.textureRect().origin,
                                                     size: CGSize(width: shirtRightArm.frame.size.width/texture.size().width,
                                                                  height: shirtRightArm.frame.size.height/texture.size().height)),
                                                    in: texture)
                sraTexts.append(sraText)
                let slaText = SKTexture(rect: CGRect(origin: texture.textureRect().origin,
                                                     size: CGSize(width: shirtLeftArm.frame.size.width/texture.size().width,
                                                                  height: shirtLeftArm.frame.size.height/texture.size().height)),
                                                    in: texture)
                slaTexts.append(slaText)
                let sralText = SKTexture(rect: CGRect(origin: texture.textureRect().origin,
                                                     size: CGSize(width: shirtRightArmLower.frame.size.width/texture.size().width,
                                                                  height: shirtRightArmLower.frame.size.height/texture.size().height)),
                                        in: texture)
                sralTexts.append(sralText)
                let slalText = SKTexture(rect: CGRect(origin: texture.textureRect().origin,
                                                     size: CGSize(width: shirtLeftArmLower.frame.size.width/texture.size().width,
                                                                  height: shirtLeftArmLower.frame.size.height/texture.size().height)),
                                        in: texture)
                slalTexts.append(slalText)
            }
            
            shirtMain.isPaused = false
            shirtRightArm.isPaused = false
            shirtShoulders.isPaused = false
            shirtLeftArm.isPaused = false
            shirtRightArmLower.isPaused = false
            shirtLeftArmLower.isPaused = false
            self.isPaused = false
            
            shirtMain.texture = smTexts.first!
            shirtShoulders.texture = ssTexts.first!
            shirtRightArm.texture = sraTexts.first!
            shirtLeftArm.texture = slaTexts.first!
            shirtRightArmLower.texture = sralTexts.first!
            shirtLeftArmLower.texture = slalTexts.first!
            
            shirtMain.run(SKAction.repeatForever(SKAction.animate(with: smTexts, timePerFrame: 0.1, resize: false, restore: false)))
            shirtShoulders.run(SKAction.repeatForever(SKAction.animate(with: ssTexts, timePerFrame: 0.1, resize: false, restore: false)))
            shirtLeftArm.run(SKAction.repeatForever(SKAction.animate(with: slaTexts, timePerFrame: 0.1, resize: false, restore: false)))
            shirtRightArm.run(SKAction.repeatForever(SKAction.animate(with: sraTexts, timePerFrame: 0.1, resize: false, restore: false)))
            shirtLeftArmLower.run(SKAction.repeatForever(SKAction.animate(with: slalTexts, timePerFrame: 0.1, resize: false, restore: false)))
            shirtRightArmLower.run(SKAction.repeatForever(SKAction.animate(with: sralTexts, timePerFrame: 0.1, resize: false, restore: false)))
            
        }
    }
    
    var legsTextures: [SKTexture]! {
        didSet {
            
            var lhTexts = [SKTexture]()
            var lllTexts = [SKTexture]()
            var lrlTexts = [SKTexture]()
            var llllTexts = [SKTexture]()
            var lrllTexts = [SKTexture]()
            
            for texture in legsTextures
            {
                let lhText = SKTexture(rect: CGRect(origin: texture.textureRect().origin,
                                                    size: CGSize(width: legsHips.frame.size.width/texture.size().width,
                                                                 height: legsHips.frame.size.height/texture.size().height)),
                                       in: texture)
                lhTexts.append(lhText)
                let lrlText = SKTexture(rect: CGRect(origin: texture.textureRect().origin,
                                                    size: CGSize(width: legsRightLeg.frame.size.width/texture.size().width,
                                                                 height: legsRightLeg.frame.size.height/texture.size().height)),
                                       in: texture)
                lrlTexts.append(lrlText)
                let lllText = SKTexture(rect: CGRect(origin: texture.textureRect().origin,
                                                     size: CGSize(width: legsLeftLeg.frame.size.width/texture.size().width,
                                                                  height: legsLeftLeg.frame.size.height/texture.size().height)),
                                        in: texture)
                lllTexts.append(lllText)
                let lrllText = SKTexture(rect: CGRect(origin: texture.textureRect().origin,
                                                     size: CGSize(width: legsRightLegLower.frame.size.width/texture.size().width,
                                                                  height: legsRightLegLower.frame.size.height/texture.size().height)),
                                        in: texture)
                lrllTexts.append(lrllText)
                let llllText = SKTexture(rect: CGRect(origin: texture.textureRect().origin,
                                                     size: CGSize(width: legsLeftLegLower.frame.size.width/texture.size().width,
                                                                  height: legsLeftLegLower.frame.size.height/texture.size().height)),
                                        in: texture)
                llllTexts.append(llllText)
            }
            
            legsHips.isPaused = false
            legsLeftLeg.isPaused = false
            legsRightLeg.isPaused = false
            legsRightLegLower.isPaused = false
            legsLeftLegLower.isPaused = false
            self.isPaused = false
            
            legsHips.texture = lhTexts.first!
            legsLeftLeg.texture = lllTexts.first!
            legsRightLeg.texture = lrlTexts.first!
            legsRightLegLower.texture = lrllTexts.first!
            legsLeftLegLower.texture = llllTexts.first!
 
            legsHips.run(SKAction.repeatForever(SKAction.animate(with: lhTexts, timePerFrame: 0.1, resize: false, restore: false)))
            legsLeftLeg.run(SKAction.repeatForever(SKAction.animate(with: lllTexts, timePerFrame: 0.1, resize: false, restore: false)))
            legsRightLeg.run(SKAction.repeatForever(SKAction.animate(with: lrlTexts, timePerFrame: 0.1, resize: false, restore: false)))
            legsLeftLegLower.run(SKAction.repeatForever(SKAction.animate(with: llllTexts, timePerFrame: 0.1, resize: false, restore: false)))
            legsRightLegLower.run(SKAction.repeatForever(SKAction.animate(with: lrllTexts, timePerFrame: 0.1, resize: false, restore: false)))
        }
    }
    
    var hairTextures: [SKTexture]! {
        didSet {
            
            var hairTexts = [SKTexture]()
            
            hair.isPaused = false
            self.isPaused = false
            
            hair.texture = hairTextures.first!
            
            hair.run(SKAction.repeatForever(SKAction.animate(with: hairTextures, timePerFrame: 0.1, resize: false, restore: false)))
        }
    }
    
    func didMoveToScene() {
        joints = [SKPhysicsJoint]()
        parts = [SKNode]()
        
        torso =  childNode(withName: "torso")
        head = torso.childNode(withName: "head")
        hair = head.childNode(withName: "hair") as! SKSpriteNode
        shoulders = torso.childNode(withName: "shoulders")
        hips = torso.childNode(withName: "hips")
        
        
        rightArmUpper = shoulders.childNode(withName: "right_arm_upper")
        rightArmLower = rightArmUpper.childNode(withName: "right_arm_lower")
        rightHand = rightArmLower.childNode(withName: "right_hand")
        
        leftArmUpper = shoulders.childNode(withName: "left_arm_upper")
        leftArmLower = leftArmUpper.childNode(withName: "left_arm_lower")
        leftHand = leftArmLower.childNode(withName: "left_hand")
        
        rightLegUpper = hips.childNode(withName: "right_leg_upper")
        rightLegLower = rightLegUpper.childNode(withName: "right_leg_lower")
        rightFoot = rightLegLower.childNode(withName: "right_foot")
        
        leftLegUpper = hips.childNode(withName: "left_leg_upper")
        leftLegLower = leftLegUpper.childNode(withName: "left_leg_lower")
        leftFoot = leftLegLower.childNode(withName: "left_foot")
        
        var smTemp = torso.childNode(withName: "shirtMain") as! SKSpriteNode
        shirtMain = smTemp
        
        var ssTemp = shoulders.childNode(withName: "shirtShoulders") as! SKSpriteNode
        shirtShoulders = ssTemp
        
        var sraTemp = rightArmUpper.childNode(withName: "shirtRightArm") as! SKSpriteNode
        shirtRightArm = sraTemp
        
        var slaTemp = leftArmUpper.childNode(withName: "shirtLeftArm") as! SKSpriteNode
        shirtLeftArm = slaTemp
        
        shirtLeftArmLower = leftArmLower.childNode(withName: "shirtLeftArmLower") as? SKSpriteNode
        shirtRightArmLower = rightArmLower.childNode(withName: "shirtRightArmLower") as? SKSpriteNode
        
        legsLeftLeg = leftLegUpper.childNode(withName: "legsLeftLeg") as? SKSpriteNode
        legsRightLeg = rightLegUpper.childNode(withName: "legsRightLeg") as? SKSpriteNode
        legsHips = hips.childNode(withName: "legsHips") as? SKSpriteNode
        legsLeftLegLower = leftLegLower.childNode(withName: "legsLeftLegLower") as? SKSpriteNode
        legsRightLegLower = rightLegLower.childNode(withName: "legsRightLegLower") as? SKSpriteNode
        
        parts.append(contentsOf: [torso, head, shoulders, hips,
                                  shirtMain, shirtRightArm, shirtLeftArm, shirtShoulders, shirtRightArmLower, shirtLeftArmLower])
        parts.append(contentsOf: [rightArmUpper, rightArmLower, rightHand,
                                  leftArmUpper, leftArmLower, leftHand])
        parts.append(contentsOf: [rightLegLower, rightLegUpper, rightFoot,
                                  leftLegLower, leftLegUpper, leftFoot])
        
        
        torso.physicsBody = SKPhysicsBody(rectangleOf: torso.frame.size, center: CGPoint(x: 0, y: -torso.frame.size.height/2))
        torso.physicsBody!.affectedByGravity = true
        torso.physicsBody?.collisionBitMask = 9
        torso.physicsBody!.categoryBitMask = 2
        
        head.physicsBody = SKPhysicsBody(rectangleOf: head.frame.size, center: CGPoint(x: 0, y: head.frame.size.height/2))
        head.physicsBody?.affectedByGravity = true
        head.physicsBody?.collisionBitMask = 1
        head.physicsBody?.categoryBitMask = 4
        head.physicsBody!.contactTestBitMask = 8
        head.physicsBody?.pinned = false
        
        shoulders.physicsBody = SKPhysicsBody(rectangleOf: shoulders.frame.size)
        shoulders.physicsBody?.affectedByGravity = true
        shoulders.physicsBody?.collisionBitMask = 1
        shoulders.physicsBody?.categoryBitMask = 2
        
        hips.physicsBody = SKPhysicsBody(rectangleOf: hips.frame.size)
        hips.physicsBody?.affectedByGravity = true
        hips.physicsBody?.collisionBitMask = 9
        hips.physicsBody?.categoryBitMask = 2
        
        rightArmUpper.physicsBody = SKPhysicsBody(rectangleOf: rightArmUpper.frame.size, center: CGPoint(x: rightArmUpper.frame.size.width/2, y: 0))
        rightArmUpper.physicsBody?.affectedByGravity = true
        rightArmUpper.physicsBody?.collisionBitMask = 9
        rightArmUpper.physicsBody?.categoryBitMask = 2
        
        rightArmLower.physicsBody = SKPhysicsBody(rectangleOf: rightArmLower.frame.size, center: CGPoint(x: rightArmLower.frame.size.width/2, y: 0))
        rightArmLower.physicsBody?.affectedByGravity = true
        rightArmLower.physicsBody?.collisionBitMask = 9
        rightArmLower.physicsBody?.categoryBitMask = 2
        
        rightHand.physicsBody = SKPhysicsBody(rectangleOf: rightHand.frame.size, center: CGPoint(x: rightHand.frame.size.width/2, y: 0))
        rightHand.physicsBody?.affectedByGravity = true
        rightHand.physicsBody?.collisionBitMask = 9
        rightHand.physicsBody?.categoryBitMask = 4
        
        leftArmUpper.physicsBody = SKPhysicsBody(rectangleOf: leftArmUpper.frame.size, center: CGPoint(x: -leftArmUpper.frame.size.width/2, y: 0))
        leftArmUpper.physicsBody?.affectedByGravity = true
        leftArmUpper.physicsBody?.collisionBitMask = 9
        leftArmUpper.physicsBody?.categoryBitMask = 2
        
        leftArmLower.physicsBody = SKPhysicsBody(rectangleOf: leftArmLower.frame.size, center: CGPoint(x: -leftArmLower.frame.size.width/2, y: 0))
        leftArmLower.physicsBody?.affectedByGravity = true
        leftArmLower.physicsBody?.collisionBitMask = 9
        leftArmLower.physicsBody?.categoryBitMask = 2
        
        leftHand.physicsBody = SKPhysicsBody(rectangleOf: leftHand.frame.size, center: CGPoint(x: -leftHand.frame.size.width/2, y: 0))
        leftHand.physicsBody?.affectedByGravity = true
        leftHand.physicsBody?.collisionBitMask = 9
        leftHand.physicsBody?.categoryBitMask = 4
        //leftHand.physicsBody?.isDynamic = false
        
        rightLegUpper.physicsBody = SKPhysicsBody(rectangleOf: rightLegUpper.frame.size, center: CGPoint(x: rightLegUpper.frame.size.width/2, y: 0))
        rightLegUpper.physicsBody?.affectedByGravity = true
        rightLegUpper.physicsBody?.collisionBitMask = 9
        rightLegUpper.physicsBody?.categoryBitMask = 2
        
        rightLegLower.physicsBody = SKPhysicsBody(rectangleOf: rightLegLower.frame.size, center: CGPoint(x: rightLegLower.frame.size.width/2, y: 0))
        rightLegLower.physicsBody?.affectedByGravity = true
        rightLegLower.physicsBody?.collisionBitMask = 9
        rightLegLower.physicsBody?.categoryBitMask = 2
        
        rightFoot.physicsBody = SKPhysicsBody(rectangleOf: rightFoot.frame.size, center: CGPoint(x: rightFoot.frame.size.width/2, y: 0))
        rightFoot.physicsBody?.affectedByGravity = true
        rightFoot.physicsBody?.collisionBitMask = 9
        rightFoot.physicsBody?.categoryBitMask = 2
        
        leftLegUpper.physicsBody = SKPhysicsBody(rectangleOf: leftLegUpper.frame.size, center: CGPoint(x: -leftLegUpper.frame.size.width/2, y: 0))
        leftLegUpper.physicsBody?.affectedByGravity = true
        leftLegUpper.physicsBody?.collisionBitMask = 9
        leftLegUpper.physicsBody?.categoryBitMask = 2
        
        leftLegLower.physicsBody = SKPhysicsBody(rectangleOf: leftLegLower.frame.size, center: CGPoint(x: -leftLegLower.frame.size.width/2, y: 0))
        leftLegLower.physicsBody?.affectedByGravity = true
        leftLegLower.physicsBody?.collisionBitMask = 9
        leftLegLower.physicsBody?.categoryBitMask = 2
        
        leftFoot.physicsBody = SKPhysicsBody(rectangleOf: leftFoot.frame.size, center: CGPoint(x: -leftFoot.frame.size.width/2, y: 0))
        leftFoot.physicsBody?.affectedByGravity = true
        leftFoot.physicsBody?.collisionBitMask = 1
        leftFoot.physicsBody?.categoryBitMask = 2
        
        let rightHip = SKPhysicsJointPin.joint(withBodyA: hips.physicsBody!, bodyB: rightLegUpper.physicsBody!, anchor: rightLegUpper.convert(CGPoint.zero, to: scene!))
        rightHip.lowerAngleLimit = CGFloat(-100).degreesToRadians()
        rightHip.upperAngleLimit = CGFloat(0).degreesToRadians()
        rightHip.shouldEnableLimits = true
        scene!.physicsWorld.add(rightHip)
        joints.append(rightHip)
        
        let leftHip = SKPhysicsJointPin.joint(withBodyA: hips.physicsBody!, bodyB: leftLegUpper.physicsBody!, anchor: leftLegUpper.convert(CGPoint.zero, to: scene!))
        leftHip.lowerAngleLimit = CGFloat(100).degreesToRadians()
        leftHip.upperAngleLimit = CGFloat(0).degreesToRadians()
        leftHip.shouldEnableLimits = true
        scene!.physicsWorld.add(leftHip)
        joints.append(leftHip)
        
        let rightKnee = SKPhysicsJointPin.joint(withBodyA: rightLegUpper.physicsBody!, bodyB: rightLegLower.physicsBody!, anchor: rightLegLower.convert(CGPoint.zero, to: scene!))
        rightKnee.lowerAngleLimit = CGFloat(-25).degreesToRadians()
        rightKnee.upperAngleLimit = CGFloat(25).degreesToRadians()
        rightKnee.shouldEnableLimits = true
        scene!.physicsWorld.add(rightKnee)
        joints.append(rightKnee)
        
        let leftKnee = SKPhysicsJointPin.joint(withBodyA: leftLegUpper.physicsBody!, bodyB: leftLegLower.physicsBody!, anchor: leftLegLower.convert(CGPoint.zero, to: scene!))
        leftKnee.lowerAngleLimit = CGFloat(-25).degreesToRadians()
        leftKnee.upperAngleLimit = CGFloat(25).degreesToRadians()
        leftKnee.shouldEnableLimits = true
        scene!.physicsWorld.add(leftKnee)
        joints.append(leftKnee)
        
        let rightAnkle = SKPhysicsJointPin.joint(withBodyA: rightLegLower.physicsBody!, bodyB: rightFoot.physicsBody!, anchor: rightFoot.convert(CGPoint.zero, to: scene!))
        rightAnkle.lowerAngleLimit = CGFloat(-10).degreesToRadians()
        rightAnkle.upperAngleLimit = CGFloat(10).degreesToRadians()
        rightAnkle.shouldEnableLimits = true
        scene!.physicsWorld.add(rightAnkle)
        joints.append(rightAnkle)
        
        let leftAnkle = SKPhysicsJointPin.joint(withBodyA: leftLegLower.physicsBody!, bodyB: leftFoot.physicsBody!, anchor: leftFoot.convert(CGPoint.zero, to: scene!))
        leftAnkle.lowerAngleLimit = CGFloat(-10).degreesToRadians()
        leftAnkle.upperAngleLimit = CGFloat(10).degreesToRadians()
        leftAnkle.shouldEnableLimits = true
        scene!.physicsWorld.add(leftAnkle)
        joints.append(leftAnkle)
        
        let neck = SKPhysicsJointPin.joint(withBodyA: torso.physicsBody!, bodyB: head.physicsBody!, anchor: head.convert(CGPoint.zero, to: scene!))
        neck.lowerAngleLimit = CGFloat(-10).degreesToRadians()
        neck.upperAngleLimit = CGFloat(10).degreesToRadians()
        neck.shouldEnableLimits = true
        scene!.physicsWorld.add(neck)
        joints.append(neck)
        
        let shoulderPin = SKPhysicsJointFixed.joint(withBodyA: shoulders.physicsBody!, bodyB: torso.physicsBody!, anchor: shoulders.convert(CGPoint.zero, to: scene!))
        
        scene!.physicsWorld.add(shoulderPin)
        joints.append(shoulderPin)
        
        
        
        let hipPin = SKPhysicsJointPin.joint(withBodyA: hips.physicsBody!, bodyB: torso.physicsBody!, anchor: hips.convert(CGPoint.zero, to: scene!))
        hipPin.lowerAngleLimit = CGFloat(-10).degreesToRadians()
        hipPin.upperAngleLimit = CGFloat(10).degreesToRadians()
        hipPin.shouldEnableLimits = true
        scene!.physicsWorld.add(hipPin)
        joints.append(hipPin)
        
        
        
        let rightShoulder = SKPhysicsJointPin.joint(withBodyA: rightArmUpper.physicsBody!, bodyB: shoulders.physicsBody!, anchor: rightArmUpper.convert(CGPoint.zero, to: scene!))
        rightShoulder.lowerAngleLimit = CGFloat(-70).degreesToRadians()
        rightShoulder.upperAngleLimit = CGFloat(70).degreesToRadians()
        rightShoulder.shouldEnableLimits = false
        scene!.physicsWorld.add(rightShoulder)
        joints.append(rightShoulder)
        
        
        let leftShoulder = SKPhysicsJointPin.joint(withBodyA: leftArmUpper.physicsBody!, bodyB: shoulders.physicsBody!, anchor: leftArmUpper.convert(CGPoint.zero, to: scene!))
        leftShoulder.lowerAngleLimit = CGFloat(-70).degreesToRadians()
        leftShoulder.upperAngleLimit = CGFloat(70).degreesToRadians()
        leftShoulder.shouldEnableLimits = false
        scene!.physicsWorld.add(leftShoulder)
        joints.append(leftShoulder)
        
        
        let leftElbow = SKPhysicsJointPin.joint(withBodyA: leftArmUpper.physicsBody!, bodyB: leftArmLower.physicsBody!, anchor: leftArmLower.convert(CGPoint.zero, to: scene!))
        leftElbow.lowerAngleLimit = CGFloat(-160).degreesToRadians()
        leftElbow.upperAngleLimit = CGFloat(160).degreesToRadians()
        leftElbow.shouldEnableLimits = false
        scene!.physicsWorld.add(leftElbow)
        joints.append(leftElbow)
        
        
        let rightElbow = SKPhysicsJointPin.joint(withBodyA: rightArmUpper.physicsBody!, bodyB: rightArmLower.physicsBody!, anchor: rightArmLower.convert(CGPoint.zero, to: scene!))
        rightElbow.lowerAngleLimit = CGFloat(-160).degreesToRadians()
        rightElbow.upperAngleLimit = CGFloat(160).degreesToRadians()
        rightElbow.shouldEnableLimits = false
        scene!.physicsWorld.add(rightElbow)
        joints.append(rightElbow)
        
        let leftWrist = SKPhysicsJointPin.joint(withBodyA: leftArmLower.physicsBody!, bodyB: leftHand.physicsBody!, anchor: leftHand.convert(CGPoint.zero, to: scene!))
        leftWrist.lowerAngleLimit = CGFloat(-10).degreesToRadians()
        leftWrist.upperAngleLimit = CGFloat(10).degreesToRadians()
        //leftWrist.shouldEnableLimits = false
        scene!.physicsWorld.add(leftWrist)
        joints.append(leftWrist)
        
        
        let rightWrist = SKPhysicsJointPin.joint(withBodyA: rightArmLower.physicsBody!, bodyB: rightHand.physicsBody!, anchor: rightHand.convert(CGPoint.zero, to: scene!))
        rightWrist.lowerAngleLimit = CGFloat(-10).degreesToRadians()
        rightWrist.upperAngleLimit = CGFloat(10).degreesToRadians()
        //rightWrist.shouldEnableLimits = false
        scene!.physicsWorld.add(rightWrist)
        joints.append(rightWrist)
        
        
        let leftHandNode = leftHand.childNode(withName: "left_hand_node")!
        let position = leftHand.convert(leftHandNode.position, to: scene!)
        leftHandPhysicsNode = SKShapeNode(circleOfRadius: 5)
        leftHandPhysicsNode.fillColor = UIColor.clear
        leftHandPhysicsNode.strokeColor = UIColor.clear
        leftHandPhysicsNode.position = position
        leftHandPhysicsNode.physicsBody = SKPhysicsBody(circleOfRadius: leftHandPhysicsNode.frame.size.width / 2)
        leftHandPhysicsNode.physicsBody!.isDynamic = false
        leftHandPhysicsNode.physicsBody!.categoryBitMask = 4
        leftHandPhysicsNode.physicsBody!.collisionBitMask = 1
        leftHandPhysicsNode.physicsBody!.contactTestBitMask = 8
        scene!.addChild(leftHandPhysicsNode)
        let conveniencePin = SKPhysicsJointPin.joint(withBodyA: leftHandPhysicsNode.physicsBody!, bodyB: leftHand.physicsBody!, anchor: position)
        scene!.physicsWorld.add(conveniencePin)
        
        let rightHandNode = rightHand.childNode(withName: "right_hand_node")!
        let rightPosition = rightHand.convert(rightHandNode.position, to: scene!)
        rightHandPhysicsNode = SKShapeNode(circleOfRadius: 5)
        rightHandPhysicsNode.fillColor = UIColor.clear
        rightHandPhysicsNode.strokeColor = UIColor.clear
        rightHandPhysicsNode.position = rightPosition
        rightHandPhysicsNode.physicsBody = SKPhysicsBody(circleOfRadius: rightHandPhysicsNode.frame.size.width / 2)
        rightHandPhysicsNode.physicsBody!.isDynamic = false
        rightHandPhysicsNode.physicsBody!.categoryBitMask = 4
        rightHandPhysicsNode.physicsBody!.collisionBitMask = 1
        rightHandPhysicsNode.physicsBody!.contactTestBitMask = 8
        scene!.addChild(rightHandPhysicsNode)
        let rightConveniencePin = SKPhysicsJointPin.joint(withBodyA: rightHandPhysicsNode.physicsBody!, bodyB: rightHand.physicsBody!, anchor: rightPosition)
        scene!.physicsWorld.add(rightConveniencePin)
        let rightDistancePin = SKPhysicsJointLimit.joint(withBodyA: rightHandPhysicsNode.physicsBody!,
                                                         bodyB: rightHand.physicsBody!,
                                                         anchorA: rightHandPhysicsNode.position,
                                                         anchorB: rightHandNode.position)
        
        
        leftHandPhysicsNode.position.x += 50
        leftHandPhysicsNode.position.y += 85
        rightHandPhysicsNode.position.x -= 50
        rightHandPhysicsNode.position.y += 85
        
        reach = (rightArmLower.frame.width + rightArmUpper.frame.width + shoulders.frame.width + leftArmUpper.frame.width + leftArmLower.frame.width)

    }
    
    func breakApart()
    {
        for joint in joints
        {
            scene!.physicsWorld.remove(joint)
        }
        
        for part in parts
        {

        }
        
    }
    
}
