//
//  ClothesShopNode.swift
//  physicsTest
//
//  Created by Francis Conlon on 15/02/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import SpriteKit
import GameKit

class GameCenterNode: SKSpriteNode, InteractiveNode
{
    func didMoveToView()
    {
        let gameScene = scene! as! GameScene
        self.position = CGPoint(x: gameScene.visibleWidth/2 - 50,
                                y: -(scene!.size.height / 2 + (self.size.height)) + 50)
        
    }
    
    func interact()
    {
        let soundEffect = SKAction.playSoundFileNamed("buttonpress.wav", waitForCompletion: false)
        self.run(soundEffect)
        
        let gameScene = scene! as! GameScene
        
        let viewController = gameScene.view!.window!.rootViewController! as! GameViewController
        if viewController.gcEnabled {
            viewController.presentGameCenter()
        } else {
            viewController.authenticateLocalPlayer()
        }
        
        self.isUserInteractionEnabled = false
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        interact()
    }
    
    func appear()
    {
        let action = (SKAction.moveTo(y: -((scene!.size.height/2) - 50), duration: 0.4))
        action.timingMode = .easeOut
        self.run(action)
        self.isUserInteractionEnabled = true
    }
    
    func disappear()
    {
        let action = (SKAction.moveTo(y: -((scene!.size.height/2) + 50 + self.size.height), duration: 0.4))
        action.timingMode = .easeIn
        self.run(action)
        self.isUserInteractionEnabled = false
    }
    
    init()
    {
        super.init(texture: SKTexture(imageNamed: "gamecenter.png"), color: UIColor.red, size: CGSize(width: 200, height: 200))
        self.anchorPoint = CGPoint(x: 1, y: 0)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    

    
}
