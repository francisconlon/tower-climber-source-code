//
//  ClothesShopNode.swift
//  physicsTest
//
//  Created by Francis Conlon on 15/02/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import SpriteKit

class ClothesShopNode: SKSpriteNode, InteractiveNode
{
    var appeared:Bool!
    
    func didMoveToView()
    {
        let gameScene = scene! as! GameScene
        self.position = CGPoint(x: -(gameScene.visibleWidth / 2) + 50,
                                y: scene!.size.height / 2 + (self.size.height))
        
    }
    
    func interact()
    {
        let soundEffect = SKAction.playSoundFileNamed("buttonpress.wav", waitForCompletion: false)
        self.run(soundEffect)
        
        let gameScene = scene! as! GameScene
        gameScene.gameState.enter(Shopping.self)
        
        self.disappear()
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        interact()
    }
    
    func appear()
    {
        var offset: CGFloat = 0
        
        if #available(iOS 11.0, *) {
            offset = self.scene!.view!.safeAreaInsets.top
        }
        
        let action = (SKAction.moveTo(y: (scene!.size.height/2) - (80), duration: 0.4))
        action.timingMode = .easeOut
        self.run(action)
        self.isUserInteractionEnabled = true
        appeared = true
    }
    
    func disappear()
    {
        let action = (SKAction.moveTo(y: (scene!.size.height/2) + self.size.height + 10, duration: 0.4))
        action.timingMode = .easeIn
        self.run(action)
        self.isUserInteractionEnabled = false
        
        appeared = false
    }
    
    init()
    {
        super.init(texture: SKTexture(imageNamed: "clothes.png"), color: Colours.buttonBackgroundColour, size: CGSize(width: 200, height: 200))
        self.anchorPoint = CGPoint(x: 0, y: 1)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
}
