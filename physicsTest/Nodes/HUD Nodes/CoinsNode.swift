//
//  ClothesShopNode.swift
//  physicsTest
//
//  Created by Francis Conlon on 15/02/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import SpriteKit

class CoinsNode: SKSpriteNode, InteractiveNode
{
    var coinsLabel: SKLabelNode!
    var gameScene: GameScene!
    var displayedCoins: Int! {
        didSet {
            self.coinsLabel.text = String(displayedCoins)
            if displayedCoins > 999
            {
                let coins = CGFloat(displayedCoins) / 1000 as! NSNumber
                let formatter = NumberFormatter()
                formatter.maximumSignificantDigits = 3
                let coinString = formatter.string(from: coins)
                self.coinsLabel.text = String("\(coinString!)k")
            } else {
                self.coinsLabel.text = String(displayedCoins)
            }
        }
    }
    
    func didMoveToView()
    {
        gameScene = scene! as! GameScene
        self.position = CGPoint(x: (gameScene.visibleWidth / 2) - 50,
                                y: scene!.size.height / 2 + 50)
        displayedCoins = gameScene.saveDataSource.coins
    }
    
    func interact()
    {
        let soundEffect = SKAction.playSoundFileNamed("buttonpress.wav", waitForCompletion: false)
        self.run(soundEffect)
        
        self.isUserInteractionEnabled = false
        gameScene.hud.clothesShopNode.disappear()
        let coinsStoreView = CoinsStoreView(frame: CGRect(x: -gameScene.view!.frame.size.width,
                                                          y: 0,
                                                          width: gameScene.view!.frame.size.width,
                                                          height: gameScene.view!.frame.size.height))
        coinsStoreView.gameScene = gameScene
        gameScene.view!.addSubview(coinsStoreView)
    }
    
    func updateCoinsLabel()
    {
        if displayedCoins > gameScene.saveDataSource.coins {
            coinsLabel.run(SKAction.sequence([SKAction.scale(to: 0.8, duration: 0.3),
                                              SKAction.run {self.displayedCoins = self.gameScene.saveDataSource.coins},
                                              SKAction.scale(to: 1, duration: 0.2)]))
        } else if displayedCoins < gameScene.saveDataSource.coins {
            coinsLabel.run(SKAction.sequence([SKAction.scale(to: 1.2, duration: 0.3),
                                              SKAction.run {self.displayedCoins = self.gameScene.saveDataSource.coins},
                                              SKAction.scale(to: 1, duration: 0.2)]))
        } else {
            displayedCoins = gameScene.saveDataSource.coins
        }
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        interact()
    }
    
    func appear()
    {
        var offset: CGFloat = 0
        
        if #available(iOS 11.0, *) {
            offset = self.scene!.view!.safeAreaInsets.top
        }
        
        let action = (SKAction.moveTo(y: (scene!.size.height/2) - (80), duration: 0.4))
        action.timingMode = .easeOut
        self.run(action)
        self.isUserInteractionEnabled = true
    }
    
    func disappear()
    {
        let action = (SKAction.moveTo(y: (scene!.size.height/2) + self.size.height + 10, duration: 0.4))
        action.timingMode = .easeIn
        self.run(action)
        self.isUserInteractionEnabled = false
    }
    
    init()
    {
        super.init(texture: SKTexture.init(imageNamed: "coinsnode.png"), color: UIColor.red, size: CGSize(width: 489, height: 193))
        
        self.anchorPoint = CGPoint(x: 1, y: 1)
        self.coinsLabel = SKLabelNode(text: "100")
        self.coinsLabel.verticalAlignmentMode = .center
        self.coinsLabel.horizontalAlignmentMode = .right
        self.coinsLabel.fontName = "CurseCasualRegular"
        self.coinsLabel.fontSize = 90
        self.coinsLabel.position = CGPoint(x: -20, y: -size.height/2)
        self.coinsLabel.zPosition = 2
        addChild(coinsLabel)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
}
