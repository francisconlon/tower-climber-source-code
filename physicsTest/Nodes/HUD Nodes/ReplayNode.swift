//
//  ClothesShopNode.swift
//  physicsTest
//
//  Created by Francis Conlon on 15/02/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import SpriteKit

class ReplayNode: SKSpriteNode, InteractiveNode
{
    var coinsLabel: SKLabelNode!
    var gameScene: GameScene!
    
    func didMoveToView()
    {
        gameScene = scene! as! GameScene

        self.position = CGPoint(x: 0, y: -gameScene.size.height)
    }
    
    func interact()
    {
        gameScene.physicsWorld.gravity = CGVector(dx: 0, dy: -4)
        
        gameScene.person.removeAllChildren()
        gameScene.person.removeFromParent()
        gameScene.person.rightHandPhysicsNode.removeFromParent()
        gameScene.person.leftHandPhysicsNode.removeFromParent()
        let textures = gameScene.person.shirtTextures
        
        let personHolderScene = SKScene(fileNamed: "Person.sks")
        gameScene.person = personHolderScene?.childNode(withName: "Person") as! Person
        gameScene.person.move(toParent: gameScene!)
        gameScene.person.name = "person"
        gameScene.person.didMoveToScene()
        gameScene.person.shirtTextures = textures
        gameScene.gameState.enter(WaitingToStart.self)
        //disappear()
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        interact()
    }
    
    func appear()
    {
        self.run(SKAction.moveBy(x: 0, y: scene!.size.height, duration: 0.2))
        self.isUserInteractionEnabled = true
    }
    
    func disappear()
    {
        self.run(SKAction.moveBy(x: 0, y: -scene!.size.height, duration: 0.2))
        self.isUserInteractionEnabled = false
    }
    
    init()
    {
        super.init(texture: nil, color: UIColor.red, size: CGSize(width: 200, height: 125))
        
        self.anchorPoint = CGPoint(x: 0.5, y: 0.5)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
}
