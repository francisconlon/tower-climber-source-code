//
//  HUD.swift
//  physicsTest
//
//  Created by Francis Conlon on 14/02/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import SpriteKit

class HUD: SKNode, EventListenerNode
{
    var clothesShopNode: ClothesShopNode!
    var coinsNode: CoinsNode!
    var replayNode: ReplayNode!
    var gameOverNode: SKNode!
    var scoreLabel: ScoreNode!
    var gameCenterNode: GameCenterNode!
    var settingsNode: SettingsNode!
    
    func didMoveToScene()
    {


        self.zPosition = zPositions.hud
        
        scoreLabel = ScoreNode()
        addChild(scoreLabel)
        scoreLabel.didMoveToView()
        
        
        
        clothesShopNode = ClothesShopNode()
        addChild(clothesShopNode)
        clothesShopNode.didMoveToView()
        clothesShopNode.appear()
        
        coinsNode = CoinsNode()
        addChild(coinsNode)
        coinsNode.didMoveToView()
        coinsNode.appear()
        
        gameCenterNode = GameCenterNode()
        addChild(gameCenterNode)
        gameCenterNode.didMoveToView()
        gameCenterNode.appear()
        
        settingsNode = SettingsNode()
        addChild(settingsNode)
        settingsNode.didMoveToView()
        settingsNode.appear()
        
        replayNode = ReplayNode()
        addChild(replayNode)
        replayNode.didMoveToView()
        
    }
    
}
