//
//  ClothesShopNode.swift
//  physicsTest
//
//  Created by Francis Conlon on 15/02/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import SpriteKit

class ScoreNode: SKSpriteNode
{
    var scoreLabel: SKLabelNode!
    var coinsLabel: SKLabelNode!
    var scoreLabelShadow: SKLabelNode!
    var coinsLabelShadow: SKLabelNode!
    
    
    func didMoveToView()
    {
        self.position = CGPoint(x: 0, y: scene!.size.height / 2 + 50)
    }
    
    func interact()
    {
        
    }
    
    func updateForScore(score: Int)
    {
        self.scoreLabel.text = String(score)
        self.scoreLabelShadow.text = String(score)
    }
    
    func updateForCoins(coins: Int)
    {
        self.coinsLabel.text = String(coins)
        self.coinsLabelShadow.text = String(coins)
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        interact()
    }
    
    func appear()
    {
        var offset: CGFloat = 0
        
        if #available(iOS 11.0, *) {
            offset = self.scene!.view!.safeAreaInsets.top
        }
        
        let action = (SKAction.moveTo(y: (scene!.size.height/2) - (225 + offset), duration: 0.4))
        action.timingMode = .easeOut
        self.run(action)
        self.isUserInteractionEnabled = true
    }
    
    func disappear()
    {
        let action = (SKAction.moveTo(y: (scene!.size.height/2) + self.size.height + 10, duration: 0.4))
        action.timingMode = .easeIn
        self.run(action)
        self.isUserInteractionEnabled = false
    }
    
    init()
    {
        super.init(texture: nil, color: UIColor.clear, size: CGSize(width: 200, height: 125))
        
        self.anchorPoint = CGPoint(x: 0.5, y: 0)
        self.scoreLabel = SKLabelNode(text: "0")
        self.scoreLabel.fontName = "CurseCasualRegular"
        self.scoreLabel.verticalAlignmentMode = .bottom
        self.scoreLabel.horizontalAlignmentMode = .center
        self.scoreLabel.fontSize = 170
        self.scoreLabel.position = CGPoint(x :0, y: self.size.height / 2)
        addChild(scoreLabel)
        
        self.scoreLabelShadow = SKLabelNode(text: "0")
        self.scoreLabelShadow.fontName = "CurseCasualRegular"
        self.scoreLabelShadow.fontColor = SKColor.black
        self.scoreLabelShadow.verticalAlignmentMode = .bottom
        self.scoreLabelShadow.horizontalAlignmentMode = .center
        self.scoreLabelShadow.fontSize = 170
        self.scoreLabelShadow.position = CGPoint(x :7, y: -7)
        self.scoreLabelShadow.zPosition = -1
        self.scoreLabel.addChild(scoreLabelShadow)

        
        self.coinsLabel = SKLabelNode(text: "0")
        self.coinsLabel.fontName = "CurseCasualRegular"
        self.coinsLabel.verticalAlignmentMode = .center
        self.coinsLabel.horizontalAlignmentMode = .right
        self.coinsLabel.fontSize = 75
        self.coinsLabel.position = CGPoint(x: -20, y: (scoreLabel.position.y - coinsLabel.frame.size.height))
        addChild(coinsLabel)
        
        self.coinsLabelShadow = SKLabelNode(text: "0")
        self.coinsLabelShadow.fontName = "CurseCasualRegular"
        self.coinsLabelShadow.fontColor = SKColor.black
        self.coinsLabelShadow.verticalAlignmentMode = .center
        self.coinsLabelShadow.horizontalAlignmentMode = .right
        self.coinsLabelShadow.fontSize = 75
        self.coinsLabelShadow.position = CGPoint(x :5, y: -5)
        self.coinsLabelShadow.zPosition = -1
        self.coinsLabel.addChild(coinsLabelShadow)
        
        let coin = CoinNode()
        coin.setScale(0.4)
        coin.isUserInteractionEnabled = false
        coin.position = CGPoint(x: 50, y: 0)
        coinsLabel.addChild(coin)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
}
