//
//  File.swift
//  physicsTest
//
//  Created by Francis Conlon on 23/02/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import SpriteKit

class EnemyNode: SKSpriteNode
{
    var gameScene: GameScene
    var indicatorNode: SKShapeNode!
    var enemySprite: SKSpriteNode!
    var used = false
    
    init(height: Int, scene: GameScene)
    {
        gameScene = scene
        
        super.init(texture: nil, color: SKColor.clear, size: CGSize(width: 325, height: 325))
        
        self.isUserInteractionEnabled = true
        self.zPosition = 15
        
        let textureAtlas = SKTextureAtlas(named: "plane")
        var rotateFrames: [SKTexture] = []
        let numImages = textureAtlas.textureNames.count
        for i in 1...numImages
        {
            let coinName = "plane\(i)"
            rotateFrames.append(textureAtlas.textureNamed(coinName))
        }
        let firstFrameTexture = rotateFrames[0]
        enemySprite = SKSpriteNode(texture: firstFrameTexture)
        enemySprite.setScale(0.65)
        enemySprite.position = CGPoint.zero
        addChild(enemySprite)
        
        enemySprite.physicsBody = SKPhysicsBody(rectangleOf: enemySprite.size)
        enemySprite.physicsBody!.isDynamic = false
        enemySprite.physicsBody!.categoryBitMask = 8
        enemySprite.physicsBody!.contactTestBitMask = 4
        enemySprite.physicsBody!.collisionBitMask = 2
        enemySprite.zRotation = CGFloat(-5).degreesToRadians()

        
        enemySprite.run(SKAction.sequence([SKAction.wait(forDuration: 0.2, withRange: 0.05),
                                        SKAction.repeatForever(SKAction.animate(with: rotateFrames, timePerFrame: 0.4, resize: false, restore: false))]))
        let rotate = SKAction.rotate(byAngle: CGFloat(10).degreesToRadians(), duration: 1)
        let rotateBack = SKAction.rotate(byAngle: CGFloat(-10).degreesToRadians(), duration: 1)
        let sequence = SKAction.sequence([rotate, rotateBack])
        enemySprite.run(SKAction.repeatForever(sequence))
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if !used
        {
            gameScene.enemiesTapped += 1
            
            let emitterNode = SKEmitterNode(fileNamed: "CoinEnemyParticle.sks")!
            emitterNode.targetNode = gameScene
            emitterNode.zPosition = 20
            emitterNode.position = self.position
            gameScene.addChild(emitterNode)
            
            let explodeEmitter = SKEmitterNode(fileNamed: "PlaneExplosionParticle.sks")!
            explodeEmitter.targetNode = gameScene
            explodeEmitter.zPosition = 19
            explodeEmitter.position = self.position
            gameScene.addChild(explodeEmitter)
            
            enemySprite.run(SKAction.sequence([SKAction.scale(to: 0, duration: 0.2), SKAction.removeFromParent()]))
            
            self.run(SKAction.sequence([SKAction.wait(forDuration: 2),
                                        SKAction.removeFromParent()]))
            let deathSound = SKAction.playSoundFileNamed("enemydeath.wav", waitForCompletion: false)
            let sound = SKAction.playSoundFileNamed("coin.wav", waitForCompletion: false)
            self.run(deathSound)
            self.run(SKAction.sequence([SKAction.wait(forDuration: 0.2), sound]))
            used = true
        }
    }
    
}
