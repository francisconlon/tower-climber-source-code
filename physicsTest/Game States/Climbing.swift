//
//  WaitingToStart.swift
//  physicsTest
//
//  Created by Francis Conlon on 13/02/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import SpriteKit
import GameplayKit

class Climbing: GKState
{
    unowned let scene: GameScene
    
    init(scene: SKScene)
    {
        self.scene = scene as! GameScene
    }
    
    override func didEnter(from previousState: GKState?)
    {
        //scene.hud.scoreLabel.appear()
        scene.enemyInterval = 5
        scene.run(SKAction.sequence([SKAction.wait(forDuration: TimeInterval(self.scene.enemyInterval)),
                                                            SKAction.run { self.scene.spawnEnemy() }]),
                  withKey: "spawnenemy")
        scene.hud.clothesShopNode.disappear()
        scene.worldVelocity = 100
        scene.run(SKAction.repeatForever(SKAction.sequence([SKAction.wait(forDuration: 1), SKAction.run {
            if self.scene.worldVelocity < 250
            {
                self.scene.worldVelocity += 50
            } else if self.scene.worldVelocity >= 250 && self.scene.worldVelocity < 450 {
                self.scene.worldVelocity += 20
            } else if self.scene.worldVelocity >= 450 && self.scene.worldVelocity < 525 {
                self.scene.worldVelocity += 5
            } else if self.scene.worldVelocity >= 525{
                self.scene.removeAction(forKey: "worldvelocity")
            }
            
            
            }])), withKey: "worldvelocity")
    }
    
    override func willExit(to nextState: GKState) {
        scene.removeAction(forKey: "worldvelocity")
    }
}
