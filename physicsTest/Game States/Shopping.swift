//
//  WaitingToStart.swift
//  physicsTest
//
//  Created by Francis Conlon on 13/02/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import SpriteKit
import GameplayKit
import UIKit

class Shopping: GKState
{
    unowned let scene: GameScene
    
    init(scene: SKScene)
    {
        self.scene = scene as! GameScene
    }
    
    override func didEnter(from previousState: GKState?)
    {
        
        scene.person.torso.physicsBody!.isDynamic = false
        scene.person.torso.zRotation = 0
        var targetPosition = scene.person.torso.convert(scene.person.hips.position, to: scene)
        var cameraTarget = targetPosition
        cameraTarget.x += 125
        cameraTarget.y += 25
        scene.cameraNode.run(SKAction.move(to: cameraTarget, duration: 1))
        scene.cameraNode.run(SKAction.scale(to: 0.3, duration: 1))
        
        var rightHandTarget = CGPoint(x: targetPosition.x + 120, y: targetPosition.y - 40)
        //gameScene.person.rightHandPhysicsNode.run(SKAction.move(to: rightHandTarget, duration: 0.5))
        
        scene.person.rightHand.physicsBody!.isDynamic = false
        scene.person.rightArmUpper.physicsBody!.isDynamic = false
        scene.person.rightArmLower.physicsBody!.isDynamic = false
        //scene.person.rightArmUpper.zRotation = CGFloat(-45).degreesToRadians()
        scene.person.isPaused = false
        scene.person.rightArmUpper.run(SKAction.rotate(toAngle: CGFloat(-45).degreesToRadians(), duration: 0.2))
        
        
        scene.person.leftHand.physicsBody!.isDynamic = false
        scene.person.leftArmUpper.physicsBody!.isDynamic = false
        scene.person.leftArmLower.physicsBody!.isDynamic = false
        //scene.person.leftArmUpper.zRotation = CGFloat(45).degreesToRadians()
        scene.person.leftArmUpper.run(SKAction.rotate(toAngle: CGFloat(45).degreesToRadians(), duration: 0.2))
        
        scene.person.hips.physicsBody!.isDynamic = false
        //scene.person.leftArmUpper.physicsBody!.isDynamic = false
       //scene.person.leftArmLower.physicsBody!.isDynamic = false
        scene.person.hips.zRotation = CGFloat(0).degreesToRadians()
        
        var shopView = ShopView(frame: CGRect(x: 0, y: -scene.view!.frame.size.height, width: scene.view!.frame.size.width, height: scene.view!.frame.size.height),
                                saveData: scene.saveDataSource)
        shopView.gameScene = scene
        
        scene.view!.addSubview(shopView)
        
        
        for hold in scene.currentBackground.holdNodes
        {
            hold.isUserInteractionEnabled = false
        }
    }
    
    override func willExit(to nextState: GKState)
    {
        scene.hud.gameCenterNode.appear()
    }
    

    
    override func update(deltaTime seconds: TimeInterval)
    {
        
    }
    

    
}
