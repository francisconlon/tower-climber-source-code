//
//  WaitingToStart.swift
//  physicsTest
//
//  Created by Francis Conlon on 13/02/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameOver: GKState
{
    unowned let scene: GameScene
    
    init(scene: SKScene)
    {
        self.scene = scene as! GameScene
    }
    
    override func didEnter(from previousState: GKState?)
    {
        let wind = scene.childNode(withName: "wind")! as! SKAudioNode
        wind.isPaused = false
        wind.removeAction(forKey: "wind")
        wind.run(SKAction.changeVolume(to: 0, duration: 0.5))

        scene.run(SKAction.sequence([SKAction.wait(forDuration: 0.5),
                                     SKAction.run {
                                        wind.removeAllActions()
                                        wind.removeAllChildren()
                                        wind.removeFromParent()
            }]), withKey: "remove")
        
        scene.currentHold = nil
        //scene.cameraNode.run(SKAction.move(to: CGPoint(x: 0, y: 300), duration: 1))
        scene.cameraNode.afterDelay(1, runBlock: {self.scene.cameraNode.run(SKAction.move(to: CGPoint(x: 0, y: 300), duration: 1))})
        
        scene.person.breakApart()
        scene.person.isPaused = false
        scene.person.run(SKAction.sequence([SKAction.wait(forDuration: 1), SKAction.fadeOut(withDuration: 1)]))
        scene.afterDelay(2.1, runBlock: {
            var frame = self.scene.view!.frame
            frame.origin.y -= frame.size.height
            let gameOverView = GameOverView(frame: frame,
                                            saveData: self.scene.saveDataSource,
                                            holdsTapped: self.scene.score,
                                            enemiesTapped: self.scene.enemiesTapped,
                                            coinsTapped: self.scene.coins,
                                            gameSceneParam: self.scene)
            gameOverView.gameScene = self.scene
            self.scene.view!.addSubview(gameOverView)
            self.scene.hud.scoreLabel.disappear()
        })
        
        scene.hud.coinsNode.appear()
        //scene.hud.replayNode.appear()
        //scene.cameraNode.physicsBody!.collisionBitMask = 2
        

    }
    
    override func willExit(to nextState: GKState) {
        if nextState is WaitingToStart
        {
            scene.resetBackground()
            scene.cameraNode.physicsBody = nil
            scene.hud.clothesShopNode.appear()
            scene.hud.gameCenterNode.appear()
        }
    }
}
