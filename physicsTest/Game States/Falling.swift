//
//  WaitingToStart.swift
//  physicsTest
//
//  Created by Francis Conlon on 13/02/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import SpriteKit
import GameplayKit

class Falling: GKState
{
    unowned let scene: GameScene
    
    init(scene: SKScene)
    {
        self.scene = scene as! GameScene
    }
    
    override func didEnter(from previousState: GKState?)
    {
        scene.removeAction(forKey: "spawnenemy")
        let sound = SKAction.playSoundFileNamed("gameover.wav", waitForCompletion: false)
        scene.run(sound)

        
        scene.person.rightHandPhysicsNode.physicsBody!.isDynamic = true
        scene.person.leftHandPhysicsNode.physicsBody!.isDynamic = true
        scene.person.rightHandPhysicsNode.removeAction(forKey: "handmove")
        scene.person.leftHandPhysicsNode.removeAction(forKey: "handmove")
        scene.physicsWorld.gravity = CGVector(dx: 0, dy: -9.8)
        
        let wind = SKAction.playSoundFileNamed("135034__mrlindstrom__windloop6sec.wav", waitForCompletion: false)
        let windPlayer = SKAudioNode(fileNamed: "135034__mrlindstrom__windloop6sec.wav")
        windPlayer.autoplayLooped = true
        windPlayer.isPaused = false
        windPlayer.run(SKAction.sequence([SKAction.changeVolume(to: 0, duration: 0),
                                          SKAction.changeVolume(to: 1, duration: 10)]), withKey: "wind")
        windPlayer.name = "wind"
        windPlayer.isPositional = false
        scene.addChild(windPlayer)
        
        scene.run(SKAction.sequence([SKAction.wait(forDuration: 1),
                                     SKAction.run {
                                        self.scene.person.rightHandPhysicsNode.physicsBody!.isDynamic = true
                                        self.scene.person.leftHandPhysicsNode.physicsBody!.isDynamic = true
                                        self.scene.person.rightHandPhysicsNode.removeAction(forKey: "handmove")
                                        self.scene.person.leftHandPhysicsNode.removeAction(forKey: "handmove")
            }]))
        
        //scene.cameraNode.physicsBody = SKPhysicsBody(edgeLoopFrom: scene.frame)
        //scene.cameraNode.physicsBody!.collisionBitMask = 0
    }
    
    
}
