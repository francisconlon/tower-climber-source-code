//
//  WaitingToStart.swift
//  physicsTest
//
//  Created by Francis Conlon on 13/02/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import SpriteKit
import GameplayKit

class WaitingToStart: GKState
{
    unowned let scene: GameScene
    
    init(scene: SKScene)
    {
        self.scene = scene as! GameScene
    }
    
    override func didEnter(from previousState: GKState?)
    {
        scene.comboPossible = false
        scene.comboIndex = 1
        
        scene.coins = 0
        
        let startLine = SKShapeNode(rectOf: CGSize(width: scene.size.width, height: 5))
        startLine.fillColor = UIColor.clear
        startLine.position.y = scene.size.height * 0.25
        //scene.addChild(startLine)
        scene.startLine = startLine
        
        scene.currentEnemies = [EnemyNode]()
        
        scene.person.torso.physicsBody!.isDynamic = true
        scene.person.head.physicsBody?.isDynamic = true
        scene.person.torso.physicsBody!.affectedByGravity = true
        scene.person.shoulders.physicsBody?.isDynamic = true
        
        scene.person.rightHand.physicsBody!.isDynamic = true
        scene.person.rightArmUpper.physicsBody!.isDynamic = true
        scene.person.rightArmLower.physicsBody!.isDynamic = true
        
        scene.person.leftHand.physicsBody!.isDynamic = true
        scene.person.leftArmUpper.physicsBody!.isDynamic = true
        scene.person.leftArmLower.physicsBody!.isDynamic = true
        
        scene.person.hips.physicsBody!.isDynamic = true
        
        scene.score = 0
        
        if scene.rightSky != nil { scene.rightSky.removeFromParent() }
        if scene.leftSky != nil { scene.leftSky.removeFromParent() }
        
        scene.rightSky = SKSpriteNode(texture: nil, color: SKColor.init(red: 0, green: 1, blue: 1, alpha: 1), size: CGSize(width: scene.size.width * 2,
                                                                                                                           height: scene.size.height))
        scene.rightSky.position = CGPoint(x: scene.size.width/2 + scene.rightSky.size.width/2, y: scene.size.height / 2)
        scene.rightSky.physicsBody = SKPhysicsBody(edgeFrom: CGPoint(x: 0,
                                                                     y: scene.rightSky.size.height / 2),
                                                   to: CGPoint(x: 0,
                                                               y: -(scene.rightSky.size.height / 2)))
        scene.rightSky.physicsBody!.isDynamic = false
        
        scene.leftSky = SKSpriteNode(texture: nil, color: SKColor.init(red: 0, green: 1, blue: 1, alpha: 1), size: CGSize(width: scene.size.width * 2,
                                                                                                                          height: scene.size.height))
        scene.leftSky.position = CGPoint(x: -(scene.size.width/2 + scene.leftSky.size.width/2), y: scene.size.height/2)
        scene.leftSky.physicsBody = SKPhysicsBody(edgeFrom: CGPoint(x: 0,
                                                                     y: scene.leftSky.size.height / 2),
                                                   to: CGPoint(x: 0,
                                                               y: -(scene.leftSky.size.height / 2)))
        scene.leftSky.physicsBody!.isDynamic = false
        
        scene.addChild(scene.rightSky)
        scene.addChild(scene.leftSky)
        
        if previousState is Shopping
        {
            scene.cameraNode.run(SKAction.scale(to: 1, duration: 0.5))
            scene.cameraNode.run(SKAction.move(to: CGPoint(x: 0, y: 300), duration: 0.5))
            for hold in scene.currentBackground.holdNodes
            {
                hold.isUserInteractionEnabled = true
            }
            
            //scene.hud.clothesShopNode.appear()
            scene.hud.settingsNode.appear()
        }
        
        if previousState is GameOver
        {
            //scene.hud.scoreLabel.disappear()
            scene.hud.settingsNode.appear()
        }
    }
    
    override func willExit(to nextState: GKState) {
        scene.startLine.removeFromParent()
        scene.startLine = nil
        scene.hud.gameCenterNode.disappear()
        
        scene.hud.settingsNode.disappear()
        if !(nextState is Shopping)
        {
            scene.hud.coinsNode.disappear()
            
            scene.hud.scoreLabel.appear()
            scene.score = 0
            scene.coins = 0
            scene.enemiesTapped = 0
        }
    }
    

    
    override func update(deltaTime seconds: TimeInterval) {
        
    }
    
    override func isValidNextState(_ stateClass: AnyClass) -> Bool {
        return stateClass is Climbing.Type || stateClass is Shopping.Type
    }
    
}
