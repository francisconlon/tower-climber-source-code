//
//  ConsentViewController.swift
//  physicsTest
//
//  Created by Francis Conlon on 22/02/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import UIKit
import PersonalizedAdConsent

class ConsentViewController: UIViewController {

    @IBOutlet var yesButton: UIButton!
    
    @IBOutlet var noButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        for provider in PACConsentInformation.sharedInstance.adProviders!
        {
        }
    }


    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    @IBAction func noButtonPressed(_ sender: Any)
    {
        PACConsentInformation.sharedInstance.consentStatus = .nonPersonalized
        let gvc = self.presentingViewController as? GameViewController
        gvc?.configureAds()
        self.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func yesButtonPressed(_ sender: Any)
    {
        PACConsentInformation.sharedInstance.consentStatus = .personalized
        let gvc = self.presentingViewController as? GameViewController
        gvc?.configureAds()
        self.dismiss(animated: true, completion: nil)
    }
}
