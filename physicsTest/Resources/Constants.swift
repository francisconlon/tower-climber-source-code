//
//  Constants.swift
//  physicsTest
//
//  Created by Francis Conlon on 19/02/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import Foundation
import UIKit

struct Colours
{
    static let buttonBackgroundColour: UIColor = UIColor.red
}

struct PhysicsCategory
{
    
}

struct zPositions
{
    static let sky: CGFloat = 0.0
    static let skyparticles: CGFloat = 1.0
    static let tower: CGFloat = 2.0
    static let handholds: CGFloat = 3.0
    static let hands: CGFloat = 4.0
    static let forearms: CGFloat = 5.0
    static let upperarms: CGFloat = 6.0
    static let chest: CGFloat = 7.0
    static let head: CGFloat = 8.0
    static let hud: CGFloat = 100.0
}
