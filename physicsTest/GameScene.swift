//
//  GameScene.swift
//  physicsTest
//
//  Created by Francis Conlon on 11/02/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameScene: SKScene {
    
    var saveDataSource: SaveDataSource!
    lazy var gameState: GKStateMachine = GKStateMachine(states: [WaitingToStart(scene: self),
                                                                 Shopping(scene: self),
                                                                 Climbing(scene: self),
                                                                 Falling(scene: self),
                                                                 GameOver(scene: self)])
    
    // MARK: - Skeleton Properties
    
    var arms: SKNode!
    var armChain: SKNode!
    var score: Int! {
        didSet {
            if score >= 0
            {
                hud.scoreLabel.updateForScore(score: score)
            }
        }
    }
    var coins: Int! {
        didSet {
            if coins >= 0
            {
                hud.scoreLabel.updateForCoins(coins: coins)
            }
        }
    }
    
    var comboPossible: Bool = false
    var comboIndex: Int = 1
    
    var currentEnemies: [EnemyNode]!
    
    var currentHold: HoldNode!
    
    var enemiesTapped: Int = 0
    
    var tileCount: Int!
    var shoulders: SKNode!
    var hips: SKNode!
    
    var background: SKNode!
    var rightSky: SKSpriteNode!
    var leftSky: SKSpriteNode!
    
    var worldVelocity: CGFloat = 100.0
    
    var reach: CGFloat!
    var leftHandFlag = true
    
    var lastUpdateTimeInterval: TimeInterval = 0
    var deltaTime: TimeInterval = 0
    
    var levelPosition = CGPoint()
    
    let angularDamping:CGFloat = 1.0
    
    // MARK: - Background Properties
    
    var holdNodes = [HoldNode]()
    var currentBackground: BackgroundNode!
    
    // MARK: - Other properites
    
    let cameraNode = SKCameraNode()
    var startLine: SKShapeNode!
    var groundNode: SKNode!
    var hud: HUD!
    var person: Person!
    
    var backgrounds = [BackgroundNode]()
    var displayedBackgrounds = [BackgroundNode]()
    
    var visibleWidth: CGFloat!
    
    var enemyInterval: Double = 5
    
    // MARK: - View Lifecycle Methods
    
    override func sceneDidLoad()
    {
        
        
    }
    

    override func didMove(to view: SKView)
    {

        let width = getVisibleScreen(sceneWidth: scene!.size.width, sceneHeight: scene!.size.height, viewWidth: view.bounds.width, viewHeight: view.bounds.height)
        visibleWidth = width.size.width
        
        background = childNode(withName: "Background")

        let personHolderScene = SKScene(fileNamed: "Person.sks")
        person = personHolderScene?.childNode(withName: "Person") as! Person
        person.move(toParent: scene!)
        person.name = "person"
        person.didMoveToScene()
        reach = person.reach
        
        tileCount = 1
        
        let backgroundNew = BackgroundNode(size: scene!.size, pathStart: person.rightHandPhysicsNode.position, reach: reach, count: tileCount)
        tileCount += 1
        
        backgrounds.append(backgroundNew)
        displayedBackgrounds.append(backgroundNew)
        currentBackground = backgroundNew
        
        if currentBackground != nil
        {
            backgroundNew.lastHoldOfPrevious = currentBackground.lastHoldOfCurrent
        }
        
        backgroundNew.position = CGPoint.zero
        addChild(backgroundNew)
        backgroundNew.initialBackgroundSetup()
        
        currentBackground = backgroundNew
        levelPosition.y += backgroundNew.frame.height
        
        physicsWorld.contactDelegate = self
        
        addChild(cameraNode)
        camera = cameraNode
        cameraNode.position.y += 300
        
        groundNode = childNode(withName: "ground")
        
        hud = HUD()
        cameraNode.addChild(hud)
        hud.didMoveToScene()

        
        let array = NSArray(contentsOfFile: Bundle.main.path(forResource: "Tops", ofType: ".plist")!)!
        let dict = array[saveDataSource.selectedTop] as! NSDictionary
        let frames = dict["Frames"] as! NSArray
        var frameTextures = [SKTexture]()
        for frame in frames
        {
            let image = UIImage(named: "\(frame).png")!
            let text = SKTexture(image: image)
            frameTextures.append(text)
        }
        
        person.shirtTextures = frameTextures
        
        let arrayLegs = NSArray(contentsOfFile: Bundle.main.path(forResource: "Tops", ofType: ".plist")!)!
        let dictLegs = arrayLegs[saveDataSource.selectedLegs] as! NSDictionary
        let framesLegs = dictLegs["Frames"] as! NSArray
        var frameTexturesLegs = [SKTexture]()
        for frame in framesLegs
        {
            let image = UIImage(named: "\(frame).png")!
            let text = SKTexture(image: image)
            frameTexturesLegs.append(text)
        }
        
        person.legsTextures = frameTexturesLegs
        
        let arrayHead = NSArray(contentsOfFile: Bundle.main.path(forResource: "Heads", ofType: ".plist")!)!
        let dictHeads = arrayHead[saveDataSource.selectedHead] as! NSDictionary
        let framesHead = dictHeads["Frames"] as! NSArray
        var frameTexturesHeads = [SKTexture]()
        for frame in framesHead
        {
            let image = UIImage(named: "\(frame).png")!
            let text = SKTexture(image: image)
            frameTexturesHeads.append(text)
        }
        
        person.hairTextures = frameTexturesHeads
        
        gameState.enter(WaitingToStart.self)
    }

    
    // MARK: - Touch Handling
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        let nodes = scene!.nodes(at: touches.first!.location(in: scene!))
        for node in nodes
        {
            if node.name == "HoldNode"
            {
                let holdTwo = node as! HoldNode
                    if holdTwo.used == false
                    {
                        holdTwo.interact()
                    }
                
            } else if node.name == "CoinNode" {
                let coin = node as! CoinNode
                if coin.used == false
                {
                    coin.interact()
                }
            }
            
        }
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {

    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?)
    {
        
    }
    
    // MARK: - Other bits
    
    func move(_ hand: SKNode)
    {
        person.torso.physicsBody!.isDynamic = false
        hand.run(SKAction.moveBy(x: 0, y: 100, duration: 0.5))
        person.torso.run(SKAction.moveBy(x: 0, y: 50, duration: 0.5))
    }
    
    override func update(_ currentTime: TimeInterval)
    {
        if lastUpdateTimeInterval > 0
        {
            deltaTime = currentTime - lastUpdateTimeInterval
        } else {
            deltaTime = 0
        }
        lastUpdateTimeInterval = currentTime
        
        if isPaused { return }
        
        switch gameState.currentState {
        case is WaitingToStart:
            checkStartCondition()
            updateCameraXPosition(deltaTime)
        case is Climbing:
            updateCameraYPosition(deltaTime)
            updateCameraXPosition(deltaTime)
            checkFailCondition()
            updateEnemyIndicators()
        case is Falling:
            updateCameraYPosition()
            updateCameraXPositionFalling()
            
            
        default:
            break
        }
        
        //updateCamera()
        
        updateBackground()
    }
    
    func updateEnemyIndicators()
    {
    }
    
    func spawnEnemy()
    {
        let enemy = EnemyNode(height: Int(levelPosition.y), scene: self)
        let offsetY = worldVelocity *  2.5     //CGFloat.random(min: scene!.size.height, max: scene!.size.height+10)
        let int = Int.random(min: 0, max: 1)
        if int == 0
        {
            let rightHandPosition = person.convert(person.rightHandPhysicsNode.position, to: scene!)
            
            enemy.position = CGPoint(x: rightHandPosition.x + scene!.size.width,
                                     y: cameraNode.position.y + offsetY)
        } else {
            let leftHandPosition = person.convert(person.leftHandPhysicsNode.position, to: scene!)
            enemy.position = CGPoint(x: leftHandPosition.x - scene!.size.width,
                                     y: cameraNode.position.y + offsetY)
            enemy.xScale = -enemy.xScale
        }
        currentEnemies.append(enemy)
        addChild(enemy)
        
        if int == 0 {
            enemy.run(SKAction.sequence([SKAction.moveBy(x: -scene!.size.width * 2, y: 0, duration: 5),
                                         SKAction.removeFromParent()]),
                      withKey: "move")
        } else {
            enemy.run(SKAction.sequence([SKAction.moveBy(x: scene!.size.width * 2, y: 0, duration: 5),
                                         SKAction.removeFromParent()]),
                      withKey: "move")
        }
        
        if enemyInterval > 4
        {
            enemyInterval -= 0.5
        } else if enemyInterval < 4 && enemyInterval > 3.1 {
            enemyInterval -= 0.1
        }
        self.removeAction(forKey: "spawnenemy")
        self.run(SKAction.sequence([SKAction.wait(forDuration: enemyInterval),
                                    SKAction.run {
                                        self.spawnEnemy()
            }
                                    ]), withKey: "spawnenemy")
    }
    
    // MARK: - Check conditions
    
    func checkFailCondition()
    {
        if person.rightHandPhysicsNode.position.y < (cameraNode.position.y - size.height / 2) && person.leftHandPhysicsNode.position.y < (cameraNode.position.y - size.height / 2)
        {
            // Fail
            gameState.enter(Falling.self)
        }
    }
    
    func checkStartCondition()
    {
        let rightDistance = startLine.position.y - person.rightHandPhysicsNode.position.y
        let leftDistance = startLine.position.y - person.leftHandPhysicsNode.position.y
        
        if leftDistance < 0 || rightDistance < 0
        {
            gameState.enter(Climbing.self)
        }
    }
    
    
    // MARK: - Camera Methods
    
    func updateCameraYPosition()
    {
        let cameraTarget = person.convert(person.torso.position, to: scene!)
        var targetPositionY = cameraTarget.y + (size.height * 0.2)
        //let lavaPos = convert(lava.position, from: fgNode)
        //targetPositionY = max(targetPositionY, lavaPos.y)
        let diff = targetPositionY - camera!.position.y
        let cameraLagFactor: CGFloat = 0.5
        let lagDiff = diff * cameraLagFactor
        let newCameraPositionY = camera!.position.y + lagDiff
        camera!.position.y = newCameraPositionY
        //camera!.position.y = cameraTarget.y
    }
    
    func updateCameraXPositionFalling()
    {
        let cameraTarget = person.convert(person.torso.position, to: scene!)
        var targetPositionX = cameraTarget.x // - (size.height * 0.1)
        //let lavaPos = convert(lava.position, from: fgNode)
        //targetPositionY = max(targetPositionY, lavaPos.y)
        let diff = targetPositionX - camera!.position.x
        let cameraLagFactor: CGFloat = 0.2
        let lagDiff = diff * cameraLagFactor
        let newCameraPositionX = camera!.position.x + lagDiff
        camera!.position.x = newCameraPositionX
    }
    
    func updateCameraXPosition(_ dt: TimeInterval)
    {
 
        let cameraXVelocity:CGFloat = 325
        
            let cameraTarget = (person.rightHandPhysicsNode.position + person.leftHandPhysicsNode.position)/2
            var targetPositionX = cameraTarget.x // - (size.height * 0.1)
        

        if currentHold != nil
        {
            let target = currentHold.parent!.convert(currentHold.position, to: scene!)
            
            var cameraStep = cameraXVelocity * CGFloat(dt)
            
            let diff = target.x - camera!.position.x
            if diff < 0 {cameraStep = -cameraStep}
            
            if diff.magnitude > 25 {camera?.position.x += cameraStep}
            
            
            let cameraLagFactor: CGFloat = 0.5
            let lagDiff = diff * cameraLagFactor
            let newCameraPositionX = camera!.position.x + lagDiff
            //camera!.position.x = newCameraPositionX
        }
        
        
    }
    
    func updateCameraYPosition(_ dt: TimeInterval)
    {
        let bottomOfScreenY = camera!.position.y - (size.height / 2)
        //let bottomOfScreenYFg = convert(CGPoint(x: 0, y: bottomOfScreenY),
        //to: fgNode).y
        let cameraVelocityY = worldVelocity
        let cameraStep = cameraVelocityY * CGFloat(dt)
        var newCameraPositionY = camera!.position.y + cameraStep
        //newCameraPositionY = max(newCameraPositionY, bottomOfScreenYFg - 125)
        camera!.position.y = newCameraPositionY
        
    }
    
    
    // MARK: - Background handling
    
    func resetBackground()
    {
        for background in backgrounds
        {
            background.removeFromParent()
        }
        tileCount = 1
        
        let backgroundNew = BackgroundNode(size: scene!.size, pathStart: person.rightHandPhysicsNode.position, reach: reach, count: tileCount)
        tileCount += 1
        addChild(backgroundNew)
        backgroundNew.initialBackgroundSetup()
        backgrounds = [BackgroundNode]()
        backgrounds.append(backgroundNew)
        
        levelPosition.y = backgroundNew.size.height
        currentBackground = backgroundNew
        displayedBackgrounds = [BackgroundNode]()
        displayedBackgrounds.append(backgroundNew)
    }
    
    
    func setupRandomBackground() -> BackgroundNode
    {
        let backgroundNew = BackgroundNode(size: scene!.size, pathStart: currentBackground!.lastHoldOfCurrent.position, reach: reach, count: tileCount)
        backgroundNew.maxReach = reach
        backgroundNew.zPosition = zPositions.tower
        
        if currentBackground != nil
        {
            backgroundNew.lastHoldOfPrevious = currentBackground.lastHoldOfCurrent
        }
        
        backgroundNew.setupBackground()
        currentBackground = backgroundNew
        return backgroundNew
        
    }
    func updateBackground()
    {
        if leftSky != nil {leftSky.position.y = cameraNode.position.y}
        if rightSky != nil {rightSky.position.y = cameraNode.position.y}
        var height = 1 - (cameraNode.position.y / CGFloat(20000))
        if height > 1 {height = 1} else if height < 0 {height = 0}
        
        var blue = 1 - ((cameraNode.position.y - 20000) / CGFloat(20000))
        if blue > 1 {blue = 1} else if blue < 0 {blue = 0}
        let color = UIColor(red: 0, green: height, blue: blue, alpha: 1)
        leftSky.color = color
        rightSky.color = color
        
        
        let distance =  (levelPosition.y) - (camera!.position.y + size.height/2)
        //distance)
        if distance < 10
        {
            var positionStart = currentBackground.convert(currentBackground.holdNodes.last!.position, to: scene!)
            let height = currentBackground.size.height
            positionStart.y -= levelPosition.y
            let backgroundNew = BackgroundNode(size: scene!.size, pathStart: positionStart, reach: reach, count: tileCount)
            tileCount += 1
            backgroundNew.maxReach = reach
            backgroundNew.zPosition = zPositions.tower
            
            backgroundNew.position.y = levelPosition.y
            addChild(backgroundNew)
            for hold in backgroundNew.holdNodes
            {
                hold.isPaused = false
                hold.isUserInteractionEnabled = true
            }
            
            backgrounds.append(backgroundNew)
            displayedBackgrounds.append(backgroundNew)

            
            backgroundNew.setupBackground()
            
            currentBackground = backgroundNew
            levelPosition.y += backgroundNew.frame.height
        }
        
        for background in backgrounds
        {
            if gameState.currentState is Climbing
            {
                if background.position.y < (levelPosition.y - background.size.height * 2)
                {
                    background.removeFromParent()
                }
            } else if gameState.currentState is Falling {
                if (cameraNode.position.y - (scene!.size.height/2)) - (background.position.y + scene!.size.height) < 10 && background.parent == nil
                {
                    addChild(background)
                }
                
                if ((cameraNode.position.y + (scene!.size.height/2)) < background.position.y) && background.parent != nil
                {
                    background.removeFromParent()
                }
                
            }
        }
    }
    

    // MARK: - Helper Methods
    
    func getVisibleScreen(sceneWidth: CGFloat, sceneHeight: CGFloat, viewWidth: CGFloat, viewHeight: CGFloat) -> CGRect
    {
        var x: CGFloat = 0
        var y: CGFloat = 0
        var sWidth = sceneWidth
        var sHeight = sceneHeight
        
        let deviceAspectRatio = viewWidth/viewHeight
        let sceneAspectRatio = sceneWidth/sceneHeight
        
        //If the the device's aspect ratio is smaller than the aspect ratio of the preset scene dimensions, then that would mean that the visible width will need to be calculated
        //as the scene's height has been scaled to match the height of the device's screen. To keep the aspect ratio of the scene this will mean that the width of the scene will extend
        //out from what is visible.
        //The opposite will happen in the device's aspect ratio is larger.
        if deviceAspectRatio < sceneAspectRatio {
            let newSceneWidth: CGFloat = (sWidth * viewHeight) / sceneHeight
            let sceneWidthDifference: CGFloat = (newSceneWidth - viewWidth)/2
            let diffPercentageWidth: CGFloat = sceneWidthDifference / (newSceneWidth)
            
            //Increase the x-offset by what isn't visible from the lrft of the scene
            x = diffPercentageWidth * sceneWidth
            //Multipled by 2 because the diffPercentageHeight is only accounts for one side(e.g right or left) not both
            sWidth = sWidth - (diffPercentageWidth * 2 * sWidth)
        } else {
            let newSceneHeight: CGFloat = (sceneHeight * viewWidth) / sceneWidth
            let sceneHeightDifference: CGFloat = (newSceneHeight - viewHeight)/2
            let diffPercentageHeight: CGFloat = abs(sceneHeightDifference / (newSceneHeight))
            
            //Increase the y-offset by what isn't visible from the bottom of the scene
            y = diffPercentageHeight * sceneHeight
            //Multipled by 2 because the diffPercentageHeight is only accounts for one side(e.g top or bottom) not both
            sHeight = sHeight - (diffPercentageHeight * 2 * sHeight)
        }
        
        let visibleScreenOffset = CGRect(x: CGFloat(x), y: CGFloat(y), width: CGFloat(sWidth), height: CGFloat(sHeight))
        return visibleScreenOffset
    }
    
}

extension GameScene: SKPhysicsContactDelegate
{
    func didBegin(_ contact: SKPhysicsContact)
    {
        if gameState.currentState is Falling && (contact.bodyA.categoryBitMask != 8) && (contact.bodyB.categoryBitMask != 8)
        {
            gameState.enter(GameOver.self)
            self.run(SKAction.playSoundFileNamed("434781__stephenbist__luggage-drop-1.wav", waitForCompletion: false))
            self.run(SKAction.playSoundFileNamed("321486__dslrguide__plastic-crunch.wav", waitForCompletion: false))
        }
        
        var firstBody = SKPhysicsBody()
        var secondBody = SKPhysicsBody()
        if contact.bodyA.categoryBitMask > contact.bodyB.categoryBitMask {
            secondBody = contact.bodyA
            firstBody = contact.bodyB
        } else {
            firstBody = contact.bodyA
            secondBody = contact.bodyB
        }
        
        if firstBody.categoryBitMask == 4 && secondBody.categoryBitMask == 8
        {
            if gameState.currentState is Climbing
            {
                person.rightHandPhysicsNode.removeAction(forKey: "handmove")
                person.leftHandPhysicsNode.removeAction(forKey: "handmove")
                person.leftHandPhysicsNode.physicsBody!.isDynamic = true
                person.rightHandPhysicsNode.physicsBody!.isDynamic = true
                
                gameState.enter(Falling.self)
            }
            
            let enemyNode = secondBody.node!
            enemyNode.run(SKAction.sequence([SKAction.wait(forDuration: 1),
                                                    SKAction.run {
                                                        enemyNode.physicsBody!.collisionBitMask = 0
                }]))
        }
    }
}

protocol InteractiveNode
{
    func interact()
}

protocol EventListenerNode
{
    func didMoveToScene()
}
