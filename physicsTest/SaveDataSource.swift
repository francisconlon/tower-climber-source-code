//
//  HeadDataSource.swift
//  physicsTest
//
//  Created by Francis Conlon on 15/02/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import UIKit
import Foundation
import CloudKit

class SaveDataSource: NSObject
{
    var coins: Int
    var unlockedTops: [Int]
    var selectedTop: Int
    
    var unlockedLegs: [Int]
    var selectedLegs: Int
    
    var unlockedHeads: [Int]
    var selectedHead: Int
    
    var heighestHeight: Int
    var mostEnemies: Int
    var mostCoins: Int
    
    var backgroundMusic: Bool
    
    var fileURL: URL!
    
    var iCloudEnabled: Bool = true
    
    override init()
    {
        let fm = FileManager.default
        let directory = fm.urls(for: .libraryDirectory, in: .userDomainMask).first!
       
        
        let saveURL = directory.appendingPathComponent("Saves")
        fileURL = saveURL.appendingPathComponent("Save")
        
        if FileManager.default.fileExists(atPath: fileURL.path)
        {
            let data = NSMutableDictionary(contentsOfFile: fileURL.path)!
            coins = data["coins"] as! Int
            unlockedTops = data["unlockedTops"] as! Array
            selectedTop = data["selectedTop"] as! Int
            unlockedLegs = data["unlockedLegs"] as! Array
            selectedLegs = data["selectedLegs"] as! Int
            unlockedHeads = data["unlockedHeads"] as! Array
            selectedHead = data["selectedHead"] as! Int
            heighestHeight = data["heighestHeight"] as! Int
            mostEnemies = data["mostEnemies"] as! Int
            mostCoins = data["mostCoins"] as! Int
            backgroundMusic = data["backgroundMusic"] as! Bool
        } else {
            do { try fm.createDirectory(atPath: saveURL.path, withIntermediateDirectories: true, attributes: nil) } catch let error as NSError {
            }
            
            let data = NSMutableDictionary()
            data.setValue(100, forKey: "coins")
            data.setValue([0,1,2,3,4], forKey: "unlockedTops")
            data.setValue(2, forKey: "selectedTop")
            data.setValue([0,1,2,3,4], forKey: "unlockedLegs")
            data.setValue(0, forKey: "selectedLegs")
            data.setValue([0,1,2,3], forKey: "unlockedHeads")
            data.setValue(0, forKey: "selectedHead")
            data.setValue(0, forKey: "heighestHeight")
            data.setValue(0, forKey: "mostEnemies")
            data.setValue(0, forKey: "mostCoins")
            data.setValue(true, forKey: "backgroundMusic")
            data.write(toFile: fileURL.path, atomically: false)
            coins = 100
            unlockedTops = [0,1,2,3,4]
            selectedTop = 2
            unlockedLegs = [0,1,2,3,4,5]
            selectedLegs = 0
            unlockedHeads = [0,1,2,3]
            selectedHead = 0
            heighestHeight = 0
            mostCoins = 0
            mostEnemies = 0
            backgroundMusic = true
        }
        
        super.init()
    }
    
    func saveChanges()
    {
        let data = NSMutableDictionary()
        data.setValue(coins, forKey: "coins")
        data.setValue(unlockedTops, forKey: "unlockedTops")
        data.setValue(selectedTop, forKey: "selectedTop")
        data.setValue(unlockedLegs, forKey: "unlockedLegs")
        data.setValue(selectedLegs, forKey: "selectedLegs")
        data.setValue(unlockedHeads, forKey: "unlockedHeads")
        data.setValue(selectedHead, forKey: "selectedHead")
        data.setValue(heighestHeight, forKey: "heighestHeight")
        data.setValue(mostCoins, forKey: "mostCoins")
        data.setValue(mostEnemies, forKey: "mostEnemies")
        data.setValue(backgroundMusic, forKey: "backgroundMusic")
        data.write(toFile: fileURL.path, atomically: false)
        
        /*if iCloudEnabled
        {
            // Back up to icloud
            let saveRecordID = CKRecord.ID(recordName: "2")
            let saveRecord = CKRecord(recordType: "SaveData", recordID: saveRecordID)
            
            saveRecord["coins"] = coins
            saveRecord["unlockedTops"] = unlockedTops
            saveRecord["selectedTop"] = selectedTop
            
            let container = CKContainer.default()
            let privateDatabase = container.privateCloudDatabase
            
            privateDatabase.save(saveRecord, completionHandler: {(record, error) in
                if let error = error {
                    return
                }
                
            })
        }*/
    }
    
}
