//
//  ShopView.swift
//  physicsTest
//
//  Created by Francis Conlon on 15/02/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import Foundation
import UIKit
import SpriteKit
import GoogleMobileAds

class GameOverView: UIView
{
    var holdsLabel: UILabel!
    var enemiesLabel: UILabel!
    var coinsLabel: UILabel!

    
    var submitScoresButton: UIButton!
    
    var replayButton: UIButton!
    
    var gameScene: GameScene
    
    var saveDataSource: SaveDataSource
    
    var advertShown = false
    
    var height: Int
    var coinsTap: Int
    var enemies: Int
    
    var first: Bool = false
    
    init(frame: CGRect, saveData: SaveDataSource, holdsTapped: Int, enemiesTapped: Int, coinsTapped: Int, gameSceneParam: GameScene) {
        
        coinsTap = coinsTapped
        height = holdsTapped
        enemies = enemiesTapped
        gameScene = gameSceneParam
        
        let gvc = gameScene.view!.window!.rootViewController as! GameViewController
        gvc.advertHolds += height
        
        saveDataSource = saveData
        super.init(frame: frame)
        
        let imageView = UIView(frame: CGRect(x: frame.width * 0.1,
                                             y: frame.height * 0.1,
                                             width: frame.width * 0.8,
                                             height: frame.height * 0.8))
        imageView.backgroundColor = UIColor(red:0.08, green:0.23, blue:0.31, alpha:1.0)
        imageView.layer.cornerRadius = 20
        imageView.layer.borderColor = UIColor(red:0.43, green:0.72, blue:0.87, alpha:1.0).cgColor
        imageView.layer.borderWidth = 6
        imageView.isUserInteractionEnabled = true
        addSubview(imageView)
        
        holdsLabel = UILabel(frame: CGRect(x: imageView.frame.size.width * 0.25,
                                           y: imageView.frame.size.height * 0.05,
                                           width: (imageView.frame.size.width * 0.5),
                                           height: imageView.frame.size.height * 0.15))
        holdsLabel.textAlignment = NSTextAlignment.center
        holdsLabel.font = UIFont(name: "CurseCasualRegular", size: 200)
        holdsLabel.text = String("Height: \(holdsTapped)")
        holdsLabel.adjustsFontSizeToFitWidth = true
        holdsLabel.baselineAdjustment = .alignCenters
        holdsLabel.textColor = UIColor.white
        holdsLabel.alpha = 0
        imageView.addSubview(holdsLabel)
        
        
        let previousBestHolds = UILabel(frame: CGRect(x: holdsLabel.frame.size.width * 0.25,
                                                      y: holdsLabel.frame.size.height,
                                                      width: holdsLabel.frame.size.width * 0.5,
                                                      height: holdsLabel.frame.size.height * 0.5))
        previousBestHolds.textAlignment = NSTextAlignment.center
        previousBestHolds.font = UIFont(name: "CurseCasualRegular", size: 250)
        previousBestHolds.adjustsFontSizeToFitWidth = true
        previousBestHolds.baselineAdjustment = .alignCenters
        previousBestHolds.textColor = UIColor.white
        previousBestHolds.text = String("Best: \(saveData.heighestHeight)")
        holdsLabel.addSubview(previousBestHolds)
            
        if holdsTapped > saveData.heighestHeight
        {
            let difference = holdsTapped - saveData.heighestHeight
            
            let differenceHolds = UILabel(frame: CGRect(x: previousBestHolds.frame.size.width,
                                                        y: 0,
                                                        width: previousBestHolds.frame.size.width,
                                                        height: previousBestHolds.frame.size.height))
            differenceHolds.textAlignment = NSTextAlignment.center
            differenceHolds.font = UIFont(name: "CurseCasualRegular", size: 20)
            differenceHolds.text = String("+\(difference)")
            differenceHolds.textColor = UIColor.green
            previousBestHolds.addSubview(differenceHolds)
            saveData.heighestHeight = holdsTapped
        }
        
        enemiesLabel = UILabel(frame: CGRect(x: holdsLabel.frame.origin.x,
                                             y: holdsLabel.frame.origin.y + holdsLabel.frame.size.height + previousBestHolds.frame.size.height,
                                             width: holdsLabel.frame.size.width,
                                             height: holdsLabel.frame.size.height))
        enemiesLabel.textAlignment = NSTextAlignment.center
        enemiesLabel.font = UIFont(name: "CurseCasualRegular", size: 200)
        enemiesLabel.text = String("Planes: \(enemiesTapped)")
        enemiesLabel.adjustsFontSizeToFitWidth = true
        enemiesLabel.baselineAdjustment = .alignCenters
        enemiesLabel.textColor = UIColor.white
        enemiesLabel.alpha = 0
        imageView.addSubview(enemiesLabel)
        
        let previousBestEnemies = UILabel(frame: CGRect(x: previousBestHolds.frame.origin.x,
                                                        y: enemiesLabel.frame.size.height,
                                                        width: previousBestHolds.frame.size.width,
                                                        height: previousBestHolds.frame.size.height))
        previousBestEnemies.textAlignment = NSTextAlignment.center
        previousBestEnemies.font = UIFont(name: "CurseCasualRegular", size: 250)
        previousBestEnemies.textColor = UIColor.white
        previousBestEnemies.adjustsFontSizeToFitWidth = true
        previousBestEnemies.baselineAdjustment = .alignCenters
        previousBestEnemies.text = String("Best: \(saveData.mostEnemies)")
        enemiesLabel.addSubview(previousBestEnemies)
        
        if enemiesTapped > saveData.mostEnemies
        {
            let difference = enemiesTapped - saveData.mostEnemies
            let differenceEnemies = UILabel(frame: CGRect(x: previousBestEnemies.frame.size.width,
                                                          y: 0,
                                                          width: previousBestEnemies.frame.size.width,
                                                          height: previousBestEnemies.frame.size.height))
            differenceEnemies.textAlignment = NSTextAlignment.center
            differenceEnemies.font = UIFont(name: "CurseCasualRegular", size: 20)
            differenceEnemies.text = String("+\(difference)")
            differenceEnemies.textColor = UIColor.green
            previousBestEnemies.addSubview(differenceEnemies)
            saveData.mostEnemies = enemiesTapped
        }
        
        if enemiesTapped > 0
        {
            let enemiesCoinsLabel = UILabel(frame: CGRect(x: enemiesLabel.frame.size.width,
                                                        y: 0,
                                                        width: (imageView.frame.size.width * 0.12),
                                                        height: enemiesLabel.frame.size.height))
            enemiesCoinsLabel.textAlignment = NSTextAlignment.center
            enemiesCoinsLabel.textAlignment = .right
            enemiesCoinsLabel.font = UIFont(name: "CurseCasualRegular", size: enemiesLabel.frame.size.height / 3)
            enemiesCoinsLabel.text = String("\(enemiesTapped * 3)")
            enemiesCoinsLabel.textColor = UIColor.green
            enemiesCoinsLabel.adjustsFontSizeToFitWidth = true
            enemiesCoinsLabel.baselineAdjustment = .alignCenters
            enemiesLabel.addSubview(enemiesCoinsLabel)
            
            let coinImage = UIImageView(frame: CGRect(x: enemiesCoinsLabel.frame.size.width + 5,
                                                      y: 0,
                                                      width: (imageView.frame.size.width * 0.08) - 5,
                                                      height: enemiesCoinsLabel.frame.size.height))
            coinImage.contentMode = .scaleAspectFit
            var coinImages = [UIImage]()
            for i in 1...6
            {
                if let image = UIImage(named: "scoin\(i).png")
                {
                    coinImages.append(image)
                }
                
            }
            coinImage.animationImages = coinImages
            coinImage.animationRepeatCount = -1
            coinImage.animationDuration = 0.75
            coinImage.startAnimating()
            enemiesCoinsLabel.addSubview(coinImage)
        }
        
        coinsLabel = UILabel(frame: CGRect(x: holdsLabel.frame.origin.x,
                                        y: enemiesLabel.frame.origin.y + enemiesLabel.frame.size.height + previousBestHolds.frame.size.height,
                                        width: holdsLabel.frame.size.width,
                                        height: holdsLabel.frame.size.height))
        coinsLabel.textAlignment = NSTextAlignment.center
        coinsLabel.font = UIFont(name: "CurseCasualRegular", size: 200)
        coinsLabel.text = String("Coins: \(coinsTapped)")
        coinsLabel.adjustsFontSizeToFitWidth = true
        coinsLabel.baselineAdjustment = .alignCenters
        coinsLabel.textColor = UIColor.white
        
        coinsLabel.alpha = 0
        imageView.addSubview(coinsLabel)
        
        
        let previousBestCoins = UILabel(frame: CGRect(x: previousBestHolds.frame.origin.x,
                                                      y: coinsLabel.frame.size.height,
                                                      width: previousBestHolds.frame.size.width,
                                                      height: previousBestHolds.frame.size.height))
        previousBestCoins.textAlignment = NSTextAlignment.center
        previousBestCoins.font = UIFont(name: "CurseCasualRegular", size: 250)
        previousBestCoins.textColor = UIColor.white
        previousBestCoins.text = String("Best: \(saveData.mostCoins)")
        previousBestCoins.adjustsFontSizeToFitWidth = true
        previousBestCoins.baselineAdjustment = .alignCenters
        coinsLabel.addSubview(previousBestCoins)
        
        if coinsTapped > saveData.mostCoins
        {
            let difference = coinsTapped - saveData.mostCoins
            let differenceCoins = UILabel(frame: CGRect(x: previousBestCoins.frame.size.width,
                                                        y: 0,
                                                        width: previousBestCoins.frame.size.width,
                                                        height: previousBestCoins.frame.size.height))
            differenceCoins.textAlignment = NSTextAlignment.center
            differenceCoins.font = UIFont(name: "CurseCasualRegular", size: 20)
            differenceCoins.text = String("+\(difference)")
            differenceCoins.textColor = UIColor.green
            previousBestCoins.addSubview(differenceCoins)
            saveData.mostCoins = coinsTapped
        }
        
        if coinsTapped > 0
        {
            let coinsCoinsLabel = UILabel(frame: CGRect(x: coinsLabel.frame.size.width,
                                                          y: 0,
                                                          width: (imageView.frame.size.width * 0.12),
                                                          height: coinsLabel.frame.size.height))
            coinsCoinsLabel.textAlignment = NSTextAlignment.center
            coinsCoinsLabel.textAlignment = .right
            coinsCoinsLabel.font = UIFont(name: "CurseCasualRegular", size: coinsLabel.frame.size.height / 3)
            coinsCoinsLabel.text = String("\(coinsTapped)")
            coinsCoinsLabel.adjustsFontSizeToFitWidth = true
            coinsCoinsLabel.baselineAdjustment = .alignCenters
            coinsCoinsLabel.textColor = UIColor.green
            coinsLabel.addSubview(coinsCoinsLabel)
            
            let coinImage = UIImageView(frame: CGRect(x: coinsCoinsLabel.frame.size.width + 5,
                                                      y: 0,
                                                      width: (imageView.frame.size.width * 0.08) - 5,
                                                      height: coinsCoinsLabel.frame.size.height))
            coinImage.contentMode = .scaleAspectFit
            var coinImages = [UIImage]()
            for i in 1...6
            {
                if let image = UIImage(named: "scoin\(i).png")
                {
                    coinImages.append(image)
                }
                
            }
            coinImage.animationImages = coinImages
            coinImage.animationRepeatCount = -1
            coinImage.animationDuration = 0.75
            coinImage.startAnimating()
            coinsCoinsLabel.addSubview(coinImage)
        }
        
        let bottomY = coinsLabel.frame.origin.y + coinsLabel.frame.size.height + previousBestCoins.frame.size.height
        let midYDifference = imageView.frame.size.height - bottomY - (imageView.frame.size.width * 0.1)
        let midYPoint = bottomY + (midYDifference / 2)
        
        submitScoresButton = UIButton(frame: CGRect(x: imageView.frame.size.width * 0.2,
                                                    y: (midYPoint - ((imageView.frame.size.width * 0.6) / 3) / 2),
                                                    width: imageView.frame.size.width * 0.6,
                                                    height: (imageView.frame.size.width * 0.6) / 3))
        submitScoresButton.setImage(UIImage(named: "submit.png"), for: .normal)
        //submitScoresButton.backgroundColor = UIColor.red
        submitScoresButton.alpha = 0
        submitScoresButton.isEnabled = false
        submitScoresButton.addTarget(self, action: #selector(GameOverView.submitHighScores), for: .touchUpInside)
        imageView.addSubview(submitScoresButton)
        
        replayButton = UIButton(frame: CGRect(x: frame.size.width * 0.4,
                                              y: imageView.frame.maxY - (frame.size.width * 0.1),
                                              width: frame.size.width * 0.2,
                                              height: frame.size.width * 0.2))
        replayButton.setImage(UIImage(named: "replaybutton.png"), for: .normal)
        replayButton.backgroundColor = UIColor.clear
        replayButton.isEnabled = false
        replayButton.addTarget(self, action: #selector(GameOverView.replayButtonPressed), for: .touchUpInside)
        addSubview(replayButton)
        
    }
    
    override func didMoveToSuperview()
    {
        let gvc = gameScene.view!.window!.rootViewController as! GameViewController
        if gvc.advertHolds > 20 && gvc.interstitial.isReady && !advertShown
        {
            SKTAudio.sharedInstance().pauseBackgroundMusic()
            advertShown = true
            gvc.interstitial.delegate = self
            gvc.interstitial.present(fromRootViewController: gvc)
            gvc.advertHolds = 0
        } else {
            UIView.animate(withDuration: 0.5,
                           delay: 0.5,
                           options: .curveEaseOut,
                           animations: {self.frame.origin.y += self.frame.size.height},
                           completion: {finished in
                            //self.replayButton.isEnabled = true
                            self.displayScores()
            })
        }
        
    }
    
    func animateHoldsLabel()
    {
        self.holdsLabel.alpha = 1
    }
    
    func displayScores()
    {
        UIView.animate(withDuration: 0.4,
                       delay: 0.5,
                       options: .curveEaseOut,
                       animations: {self.holdsLabel.alpha = 1},
                       completion: { finished in UIView.animate(withDuration: 0.4,
                                                  delay: 0.3,
                                                  options: .curveEaseOut,
                                                  animations: {self.enemiesLabel.alpha = 1},
                                                  completion: { finished in
                                                    
                                                                            UIView.animate(withDuration: 0.4,
                                                                                           delay: 0.3,
                                                                                           options: .curveEaseOut,
                                                                                           animations: {self.coinsLabel.alpha = 1}, completion: {
                                                                                            finished in
                                                                                            
                                                                                            UIView.animate(withDuration: 0.4,
                                                                                                                       delay: 0.3,
                                                                                                                       options: .curveEaseOut,
                                                                                                                       animations: {self.submitScoresButton.alpha = 1},
                                                                                                                       completion: { finished in self.replayButton.isEnabled = true
                                                                                                                        self.submitScoresButton.isEnabled = true
                                                                                                                        if self.first == false
                                                                                                                        {
                                                                                                                            self.saveDataSource.coins += (self.enemies * 3)
                                                                                                                            self.saveDataSource.coins += self.coinsTap
                                                                                                                            self.gameScene.hud.coinsNode.updateCoinsLabel()
                                                                                                                            self.saveDataSource.saveChanges()
                                                                                                                            self.first = true
                                                                                                                            
                                                                                                                            if self.coinsTap > 0 || self.enemies > 0 {
                                                                                                                                SKTAudio.sharedInstance().playSoundEffect("coin.wav")
                                                                                                                            }
                                                                                                                        }
                                                                                            }
                                                                                            )
                                                                                            
                                                  })})})
    }
    
    @objc func submitHighScores()
    {
        
        let gvc = gameScene.view!.window!.rootViewController as! GameViewController
        gvc.submitHighScores(height: height, coins: coinsTap, enemies: enemies)
        SKTAudio.sharedInstance().playSoundEffect("buttonpress.wav")
    }
    
    @objc func replayButtonPressed()
    {
        SKTAudio.sharedInstance().playSoundEffect("buttonpress.wav")
        
        gameScene.physicsWorld.gravity = CGVector(dx: 0, dy: -4)
        
        gameScene.person.removeAllChildren()
        gameScene.person.removeFromParent()
        gameScene.person.rightHandPhysicsNode.removeFromParent()
        gameScene.person.leftHandPhysicsNode.removeFromParent()
        let shirtTextures = gameScene.person.shirtTextures
        let legsTextures = gameScene.person.legsTextures
        let hairTextures = gameScene.person.hairTextures
        
        let personHolderScene = SKScene(fileNamed: "Person.sks")
        gameScene.person = personHolderScene?.childNode(withName: "Person") as! Person
        gameScene.person.move(toParent: gameScene)
        gameScene.person.name = "person"
        gameScene.person.didMoveToScene()
        gameScene.person.shirtTextures = shirtTextures
        gameScene.person.legsTextures = legsTextures
        gameScene.person.hairTextures = hairTextures
        
        UIView.animate(withDuration: 0.5,
                       delay: 0.1,
                       options: .curveEaseOut,
                       animations: {self.frame.origin.y += self.frame.size.height},
                       completion: {finished in
                        self.gameScene.gameState.enter(WaitingToStart.self)
                        self.removeFromSuperview()
        })
        
    }
    
    @objc func topButtonPressed()
    {

        
    }
    
    @objc func middleButtonPressed()
    {
    }
    
    @objc func bottomButtonPressed()
    {

    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
}

extension GameOverView: GADInterstitialDelegate
{
    func interstitialDidDismissScreen(_ ad: GADInterstitial)
    {
        if saveDataSource.backgroundMusic { SKTAudio.sharedInstance().resumeBackgroundMusic() }
        
        UIView.animate(withDuration: 0.5,
                       delay: 0.5,
                       options: .curveEaseOut,
                       animations: {self.frame.origin.y += self.frame.size.height},
                       completion: {finished in
                        //self.replayButton.isEnabled = true
                        self.displayScores()
        })
        
        let gvc = gameScene.view!.window!.rootViewController as! GameViewController
        gvc.reloadInterstitial()
    }
}
