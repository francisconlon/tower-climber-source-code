//
//  HeadDataSource.swift
//  physicsTest
//
//  Created by Francis Conlon on 15/02/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import UIKit

class LegsDataSource: NSObject, UICollectionViewDataSource
{
    var legs: NSArray!
    var saveDataSource: SaveDataSource!
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return legs.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ShopCollectionViewCell", for: indexPath) as! ShopCollectionViewCell
        
        let leg = legs[indexPath.row] as! NSDictionary
        let price = leg["Price"] as! NSNumber
        let priceInt = Int(price)
        let frames = leg["Frames"] as! NSArray
        var images = [UIImage]()
        for frame in frames
        {
            if let image = UIImage(named: "\(frame).png")
            {
                images.append(image)
            }
        }
        
        cell.textureNames = frames as! [String]
        
        cell.previewView.animationImages = images
        cell.previewView.animationRepeatCount = -1
        cell.previewView.animationDuration = 1.8
        cell.previewView.startAnimating()
        
        var coinImages = [UIImage]()
        for i in 1...6
        {
            if let image = UIImage(named: "scoin\(i).png")
            {
                coinImages.append(image)
            }
    
        }
        cell.coinImage.animationImages = coinImages
        cell.coinImage.animationRepeatCount = -1
        cell.coinImage.animationDuration = 0.75
        //cell.coinImage.startAnimating()
        
        if !saveDataSource.unlockedLegs.contains(indexPath.row)
        {
            cell.priceLabel.alpha = 1
            cell.coinImage.alpha = 1
            cell.priceLabel.text = String(priceInt)
            cell.selectedCheckMark.alpha = 0
            cell.coinImage.startAnimating()
        } else {
            cell.priceLabel.alpha = 0
            cell.coinImage.alpha = 0
            cell.selectedCheckMark.alpha = 1
            if saveDataSource.selectedLegs == indexPath.row
            {
                cell.selectedCheckMark.image = UIImage(named: "checkmarkclosed.png")
            } else {
                cell.selectedCheckMark.image = UIImage(named: "checkmarkopen.png")
            }
        }
        
        return cell
    }
    
    
    
    override init()
    {
        legs = NSArray(contentsOfFile: Bundle.main.path(forResource: "Tops", ofType: ".plist")!)
    }
    
}
