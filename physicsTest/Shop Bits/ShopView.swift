//
//  ShopView.swift
//  physicsTest
//
//  Created by Francis Conlon on 15/02/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import Foundation
import UIKit
import SpriteKit

class ShopView: UIView
{
    var topButton: UIButton!
    var middleButton: UIButton!
    var bottomButton: UIButton!
    var backButton: UIButton!
    
    var gameScene: GameScene!
    
    var currentCollectionView: UICollectionView!
    
    var headCollectionView: UICollectionView!
    var bodyCollectionView: UICollectionView!
    var legsCollectionView: UICollectionView!
    
    let headDataSource = HeadDataSource()
    let topDataSource = TopDataSource()
    let legsDataSource = LegsDataSource()
    
    var saveDataSource: SaveDataSource
    
    init(frame: CGRect, saveData: SaveDataSource) {
        
        topDataSource.saveDataSource = saveData
        legsDataSource.saveDataSource = saveData
        headDataSource.saveDataSource = saveData
        
        saveDataSource = saveData
        super.init(frame: frame)
        
        topButton = UIButton(frame: CGRect(x: self.frame.size.width / 3,
                                               y: self.frame.size.height / 4,
                                               width: self.frame.size.height * 0.06,
                                               height: (self.frame.size.height * 0.06) * 0.8))
        topButton.backgroundColor = UIColor.clear
        topButton.isEnabled = false
        topButton.setImage(UIImage(named: "forwardarrow.png"), for: .normal)
        topButton.addTarget(self, action: #selector(ShopView.topButtonPressed), for: .touchUpInside)
        addSubview(topButton)
        
        middleButton = UIButton(frame: CGRect(x: self.frame.size.width / 3,
                                           y: self.frame.size.height / 2 - 30,
                                           width: self.frame.size.height * 0.06,
                                           height: (self.frame.size.height * 0.06) * 0.8))
        middleButton.backgroundColor = UIColor.clear
        middleButton.setImage(UIImage(named: "forwardarrow.png"), for: .normal)
        middleButton.isEnabled = false
        middleButton.addTarget(self, action: #selector(ShopView.middleButtonPressed), for: .touchUpInside)
        addSubview(middleButton)
        
        bottomButton = UIButton(frame: CGRect(x: self.frame.size.width / 3,
                                           y: (self.frame.size.height / 3) * 2,
                                           width: self.frame.size.height * 0.06,
                                           height: (self.frame.size.height * 0.06) * 0.8))
        bottomButton.backgroundColor = UIColor.clear
        bottomButton.setImage(UIImage(named: "forwardarrow.png"), for: .normal)
        bottomButton.isEnabled = false
        bottomButton.addTarget(self, action: #selector(ShopView.bottomButtonPressed), for: .touchUpInside)
        addSubview(bottomButton)
        
        backButton = UIButton(frame: CGRect(x: self.frame.size.width * 0.1,
                                            y: self.frame.size.width * 0.1,
                                            width: self.frame.size.width * 0.1,
                                            height: self.frame.size.width * 0.1))
        backButton.setImage(UIImage(named: "cancelbutton.png"), for: .normal)
        backButton.backgroundColor = UIColor.clear
        backButton.isEnabled = false
        backButton.addTarget(self, action: #selector(ShopView.backButtonPressed), for: .touchUpInside)
        addSubview(backButton)
        
        let layout = UICollectionViewFlowLayout()
        layout.itemSize = CGSize(width: 155, height: 78)
        //layout.minimumInteritemSpacing = 300
        //
        
        
        let headLayout = UICollectionViewFlowLayout()
        headLayout.itemSize = CGSize(width: 155, height: 78)
        
        headCollectionView = UICollectionView(frame: CGRect(x: frame.size.width/2 - 10,
                                                          y: 100,
                                                          width: 170,
                                                          height: (frame.size.height) - 100),
                                            collectionViewLayout: UICollectionViewFlowLayout())
        headCollectionView.delegate = self
        headCollectionView.backgroundColor = UIColor.clear
        headCollectionView.register(UINib(nibName: "ShopCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ShopCollectionViewCell")
        headCollectionView.dataSource = headDataSource
        headCollectionView.isPagingEnabled = false
        headCollectionView.bounces = true
        headCollectionView.alwaysBounceVertical = true
        headCollectionView.alwaysBounceHorizontal = false
        headCollectionView.showsVerticalScrollIndicator = false
        headCollectionView.reloadSections(IndexSet(arrayLiteral: 0))
        headCollectionView.decelerationRate = .fast

        
        bodyCollectionView = UICollectionView(frame: CGRect(x: frame.size.width/2 - 10,
                                                            y: 100,
                                                            width: 170,
                                                            height: (frame.size.height) - 100),
                                              collectionViewLayout: UICollectionViewFlowLayout())
        bodyCollectionView.delegate = self
        bodyCollectionView.register(UINib(nibName: "ShopCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ShopCollectionViewCell")
        bodyCollectionView.dataSource = topDataSource
        bodyCollectionView.isPagingEnabled = false
        bodyCollectionView.bounces = true
        bodyCollectionView.alwaysBounceVertical = true
        bodyCollectionView.alwaysBounceHorizontal = false
        bodyCollectionView.showsVerticalScrollIndicator = false
        bodyCollectionView.reloadSections(IndexSet(arrayLiteral: 0))
        bodyCollectionView.backgroundColor = UIColor.clear
        bodyCollectionView.isScrollEnabled = true
        bodyCollectionView.decelerationRate = .fast
        
        legsCollectionView = UICollectionView(frame: CGRect(x: frame.size.width/2 - 10,
                                                            y: 100,
                                                            width: 170,
                                                            height: (frame.size.height) - 100),
                                              collectionViewLayout: UICollectionViewFlowLayout())
        legsCollectionView.backgroundColor = UIColor.clear
        legsCollectionView.delegate = self
        
        legsCollectionView.register(UINib(nibName: "ShopCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ShopCollectionViewCell")
        legsCollectionView.dataSource = legsDataSource
        legsCollectionView.isPagingEnabled = false
        legsCollectionView.showsVerticalScrollIndicator = false
        legsCollectionView.backgroundColor = UIColor.clear
        legsCollectionView.reloadSections(IndexSet(arrayLiteral: 0))
        legsCollectionView.decelerationRate = .fast

    }
    
    override func didMoveToSuperview() {
        
        UIView.animate(withDuration: 0.5,
                       delay: 1,
                       options: .curveEaseOut,
                       animations: {self.frame.origin.y += self.frame.size.height},
                       completion: {finished in
                        self.topButton.isEnabled = true
                        self.middleButton.isEnabled = true
                        self.bottomButton.isEnabled = true
                        self.backButton.isEnabled = true
                        //self.legsCollectionView.reloadSections(IndexSet(arrayLiteral: 0))
                        //self.headCollectionView.reloadSections(IndexSet(arrayLiteral: 0))
        })
        
    }
    
    @objc func backButtonPressed()
    {
        self.saveDataSource.saveChanges()
        
        SKTAudio.sharedInstance().playSoundEffect("buttonpress.wav")
        
        self.topButton.isEnabled = false
        self.middleButton.isEnabled = false
        self.bottomButton.isEnabled = false
        self.backButton.isEnabled = false
        UIView.animate(withDuration: 0.5,
                       delay: 0.1,
                       options: .curveEaseOut,
                       animations: {self.frame.origin.y += self.frame.size.height},
                       completion: {finished in
                        self.gameScene.hud.clothesShopNode.appear()
                        self.gameScene.gameState.enter(WaitingToStart.self)
                        self.removeFromSuperview()
        })
        
    }
    
    @objc func topButtonPressed()
    {
        if currentCollectionView == nil
        {
            addSubview(headCollectionView)
            currentCollectionView = headCollectionView
        } else if currentCollectionView == headCollectionView {
            return
        } else {
            currentCollectionView.removeFromSuperview()
            addSubview(headCollectionView)
            
            currentCollectionView = headCollectionView
        }
        //headCollectionView.reloadSections(IndexSet(arrayLiteral: 0))
        bottomButton.isHighlighted = false
        middleButton.isHighlighted = false
        
        SKTAudio.sharedInstance().playSoundEffect("buttonpress.wav")
    }
    
    @objc func middleButtonPressed()
    {
        if currentCollectionView == nil
        {
            addSubview(bodyCollectionView)
            currentCollectionView = bodyCollectionView
        } else if currentCollectionView == bodyCollectionView {
            return
        } else {
            currentCollectionView.removeFromSuperview()
            addSubview(bodyCollectionView)
            currentCollectionView = bodyCollectionView
        }
        //bodyCollectionView.reloadSections(IndexSet(arrayLiteral: 0))
        bodyCollectionView.setNeedsLayout()
        bodyCollectionView.setNeedsDisplay()
        bottomButton.isHighlighted = false
        topButton.isHighlighted = false
        
        SKTAudio.sharedInstance().playSoundEffect("buttonpress.wav")
    }
    
    @objc func bottomButtonPressed()
    {
        if currentCollectionView == nil
        {
            addSubview(legsCollectionView)
            currentCollectionView = legsCollectionView
        } else if currentCollectionView == legsCollectionView {
            return
        } else {
            currentCollectionView.removeFromSuperview()
            addSubview(legsCollectionView)
            currentCollectionView = legsCollectionView
        }
        legsCollectionView.reloadSections(IndexSet(arrayLiteral: 0))
        topButton.isHighlighted = false
        middleButton.isHighlighted = false
        
        SKTAudio.sharedInstance().playSoundEffect("buttonpress.wav")
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
}

extension ShopView: UICollectionViewDelegate
{
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath)
    {
        if collectionView == bodyCollectionView
        {
            let cell = collectionView.cellForItem(at: indexPath) as! ShopCollectionViewCell
            let selectedTop = topDataSource.tops[indexPath.row] as! NSDictionary
            let price = selectedTop["Price"] as! Int
            cell.previewView.startAnimating()
            // if is bought, equip
            if saveDataSource.unlockedTops.contains(indexPath.row)
            {
                var textures = [SKTexture]()
                
                for i in 0..<cell.textureNames.count
                {
                    let texture = SKTexture(imageNamed: "\(cell.textureNames[i]).png")
                    //let i = SKTexture(data: cell.previewView.image!.pngData(), size: <#T##CGSize#>)
                    textures.append(texture)
                }

                var index = IndexPath(row: saveDataSource.selectedTop, section: 0)
                
                
                SKTAudio.sharedInstance().playSoundEffect("buttonpress.wav")
                gameScene.person.shirtTextures = textures
                saveDataSource.selectedTop = indexPath.row
                cell.selectedCheckMark.image = UIImage(named: "checkmarkclosed")
                //collectionView.reloadData()
                //collectionView.reloadItems(at: [index])
                

                    collectionView.reloadItems(at: [index])

                

            } else if saveDataSource.coins > price {
                saveDataSource.unlockedTops.append(indexPath.row)
                saveDataSource.coins -= price
                gameScene.hud.coinsNode.updateCoinsLabel()
                cell.priceLabel.alpha = 0
                cell.coinImage.alpha = 0
                cell.selectedCheckMark.alpha = 1
                SKTAudio.sharedInstance().playSoundEffect("unlock.wav")
            } else if saveDataSource.coins < price {
                SKTAudio.sharedInstance().playSoundEffect("nounlock.wav")
            }
            
        } else if collectionView == legsCollectionView {
                let cell = collectionView.cellForItem(at: indexPath) as! ShopCollectionViewCell
                let selectedLegs = legsDataSource.legs[indexPath.row] as! NSDictionary
                let price = selectedLegs["Price"] as! Int
                cell.previewView.startAnimating()
                // if is bought, equip
                if saveDataSource.unlockedLegs.contains(indexPath.row)
                {
                    var textures = [SKTexture]()
                    
                    for i in 0..<cell.textureNames.count
                    {
                        let texture = SKTexture(imageNamed: "\(cell.textureNames[i]).png")
                        //let i = SKTexture(data: cell.previewView.image!.pngData(), size: <#T##CGSize#>)
                        textures.append(texture)
                    }
                    
                    var index = IndexPath(row: saveDataSource.selectedLegs, section: 0)
                    

                    SKTAudio.sharedInstance().playSoundEffect("buttonpress.wav")
                    gameScene.person.legsTextures = textures
                    saveDataSource.selectedLegs = indexPath.row
                    //collectionView.reloadData()
                    collectionView.reloadItems(at: [index])
                    
                    cell.selectedCheckMark.image = UIImage(named: "checkmarkclosed")
                } else if saveDataSource.coins > price {
                    saveDataSource.unlockedLegs.append(indexPath.row)
                    saveDataSource.coins -= price
                    gameScene.hud.coinsNode.updateCoinsLabel()
                    cell.priceLabel.alpha = 0
                    cell.coinImage.alpha = 0
                    cell.selectedCheckMark.alpha = 1
                    SKTAudio.sharedInstance().playSoundEffect("unlock.wav")

                } else if saveDataSource.coins < price {
                    SKTAudio.sharedInstance().playSoundEffect("nounlock.wav")

                }
        } else if collectionView == headCollectionView {
            let cell = collectionView.cellForItem(at: indexPath) as! ShopCollectionViewCell
            let selectedHead = headDataSource.heads[indexPath.row] as! NSDictionary
            let price = selectedHead["Price"] as! Int
            cell.previewView.startAnimating()
            // if is bought, equip
            if saveDataSource.unlockedHeads.contains(indexPath.row)
            {
                var textures = [SKTexture]()
                
                for i in 0..<cell.textureNames.count
                {
                    let texture = SKTexture(imageNamed: "\(cell.textureNames[i]).png")
                    //let i = SKTexture(data: cell.previewView.image!.pngData(), size: <#T##CGSize#>)
                    textures.append(texture)
                }
                
                var index = IndexPath(row: saveDataSource.selectedHead, section: 0)
                SKTAudio.sharedInstance().playSoundEffect("buttonpress.wav")
                gameScene.person.hairTextures = textures
                saveDataSource.selectedHead = indexPath.row
                //collectionView.reloadData()
                collectionView.reloadItems(at: [index])
                cell.selectedCheckMark.image = UIImage(named: "checkmarkclosed")
            } else if saveDataSource.coins > price {
                saveDataSource.unlockedHeads.append(indexPath.row)
                saveDataSource.coins -= price
                gameScene.hud.coinsNode.updateCoinsLabel()
                cell.priceLabel.alpha = 0
                cell.coinImage.alpha = 0
                cell.selectedCheckMark.alpha = 1
                SKTAudio.sharedInstance().playSoundEffect("unlock.wav")

            } else if saveDataSource.coins < price {
                SKTAudio.sharedInstance().playSoundEffect("nounlock.wav")

            }
        }

    }
    
   
   
}

extension ShopView: UICollectionViewDelegateFlowLayout
{
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
    {
        return CGSize(width: 155, height: 78)
    }
}

