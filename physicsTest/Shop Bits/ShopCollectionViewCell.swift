//
//  ShopCollectionViewCell.swift
//  physicsTest
//
//  Created by Francis Conlon on 16/02/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import UIKit

class ShopCollectionViewCell: UICollectionViewCell {

    @IBOutlet var previewView: UIImageView!
    
    @IBOutlet var coinImage: UIImageView!
    @IBOutlet var selectedCheckMark: UIImageView!
    @IBOutlet var priceLabel: UILabel!
    
    var textureNames = [String]()
    
    override open var isSelected: Bool
        {
        set {
            
        }
        
        get {
            return super.isSelected
        }
    }
    
    override open var isHighlighted: Bool
        {
        set {
            
        }
        
        get {
            return super.isHighlighted
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        previewView.layer.cornerRadius = 10
        self.isHidden = false
    }

}
