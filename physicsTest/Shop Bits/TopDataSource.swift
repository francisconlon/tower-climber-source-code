//
//  HeadDataSource.swift
//  physicsTest
//
//  Created by Francis Conlon on 15/02/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import UIKit

class TopDataSource: NSObject, UICollectionViewDataSource
{
    var tops: NSArray!
    var saveDataSource: SaveDataSource!
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int
    {
        return tops.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell
    {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ShopCollectionViewCell", for: indexPath) as! ShopCollectionViewCell
        
        let top = tops[indexPath.row] as! NSDictionary
        let price = top["Price"] as! NSNumber
        let priceInt = Int(price)
        let frames = top["Frames"] as! NSArray
        var images = [UIImage]()
        for frame in frames
        {
            if let image = UIImage(named: "\(frame).png")
            {
                images.append(image)
            }
        }
        
        cell.textureNames = frames as! [String]
        
        cell.previewView.animationImages = images
        cell.previewView.animationRepeatCount = -1
        cell.previewView.animationDuration = 1.8
        cell.previewView.startAnimating()
        
        var coinImages = [UIImage]()
        for i in 1...6
        {
            if let image = UIImage(named: "scoin\(i).png")
            {
                coinImages.append(image)
            }
            
        }
        cell.coinImage.animationImages = coinImages
        cell.coinImage.animationRepeatCount = -1
        cell.coinImage.animationDuration = 0.75
        cell.coinImage.startAnimating()

        
        if !saveDataSource.unlockedTops.contains(indexPath.row)
        {
            cell.priceLabel.alpha = 1
            cell.coinImage.alpha = 1
            cell.priceLabel.text = String(priceInt)
            cell.selectedCheckMark.alpha = 0
        } else {
            cell.priceLabel.alpha = 0
            cell.coinImage.alpha = 0
            cell.selectedCheckMark.alpha = 1
            if saveDataSource.selectedTop == indexPath.row
            {
                cell.selectedCheckMark.image = UIImage(named: "checkmarkclosed.png")
            } else {
                cell.selectedCheckMark.image = UIImage(named: "checkmarkopen.png")
            }
        }
        return cell
    }
    
    
    
    override init()
    {
        tops = NSArray(contentsOfFile: Bundle.main.path(forResource: "Tops", ofType: ".plist")!)
    }
    
}
