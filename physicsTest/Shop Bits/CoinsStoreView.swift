//
//  CoinsStoreView.swift
//  physicsTest
//
//  Created by Francis Conlon on 17/02/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import UIKit
import StoreKit
import GoogleMobileAds

class CoinsStoreView: UIView
{
    var hundredCoinsID = "com.overvoltstudios.towerclimber.100coins"
    var twoFiveCoinsID = "com.overvoltstudios.towerclimber.2500coins"
    var tenThousandCoinsID = "com.overvoltstudios.towerclimber.10000coins"
    var productIDSet = Set<String>()
    
    var backButton: UIButton!
    
    var hundredButton: UIButton!
    var thousandButton: UIButton!
    var tenThousandButton: UIButton!
    
    var hundredButtonLabel: UILabel!
    var thousandButtonLabel: UILabel!
    var tenThousandButtonLabel: UILabel!
    
    var hundredButtonPriceLabel: UILabel!
    var thousandButtonPriceLabel: UILabel!
    var tenThousandButtonPriceLabel: UILabel!
    
    var freeButton: UIButton!
    
    var hundredProduct: SKProduct!
    var thousandProduct: SKProduct!
    var tenThousandProduct: SKProduct!
    
    var products = [SKProduct]()
    
    var storeView: UIView!
    
    var gameScene: GameScene!
    
    //var productIDs = NSSet(array: ["com.overvoltstudios.towerclimber.100coins"])
    var productIDs = NSSet(objects: "com.overvoltstudios.towerclimber.100coins")
    
    
    /*
    // Only override draw() if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func draw(_ rect: CGRect) {
        // Drawing code
    }
    */
    
    override init(frame: CGRect)
    {
        super.init(frame: frame)
        self.backgroundColor = UIColor.clear
        self.isOpaque = true
        

        storeView = UIView(frame: CGRect(x: 40, y: 90, width: self.frame.size.width - 80, height: self.frame.size.height - 180))
        storeView.backgroundColor = UIColor(red:0.08, green:0.23, blue:0.31, alpha:1.0)
        storeView.layer.cornerRadius = 20
        storeView.layer.borderColor = UIColor(red:0.43, green:0.72, blue:0.87, alpha:1.0).cgColor
        storeView.layer.borderWidth = 7
        storeView.isUserInteractionEnabled = true
        addSubview(storeView)

        backButton = UIButton(frame: CGRect(x: storeView.frame.origin.x - (storeView.frame.size.width/16),
                                            y: storeView.frame.origin.y - (storeView.frame.size.width/16),
                                            width: storeView.frame.size.width/8,
                                            height: storeView.frame.size.width/8))
        backButton.backgroundColor = UIColor.clear
        backButton.isEnabled = false
        backButton.setImage(UIImage(named: "cancelbutton.png"), for: .normal)
        
        backButton.addTarget(self, action: #selector(CoinsStoreView.backButtonPressed), for: .touchUpInside)
        addSubview(backButton)
        
        
        productIDSet.insert(hundredCoinsID)
        productIDSet.insert(twoFiveCoinsID)
        productIDSet.insert(tenThousandCoinsID)
        
        // We check that we are allow to make the purchase
        if (SKPaymentQueue.canMakePayments())
        {
            var productsRequest = SKProductsRequest(productIdentifiers: productIDSet)
            productsRequest.delegate = self
            productsRequest.start()
            SKPaymentQueue.default().add(self)
        } else {
        }

        
        
        hundredButton = UIButton(frame: CGRect(x: storeView.frame.size.width * 0.2, y: storeView.frame.size.height * 0.1, width: storeView.frame.size.width * 0.6, height: (storeView.frame.size.width * 0.6) * 0.352))
        hundredButton.isEnabled = false
        hundredButton.setImage(UIImage(named: "coinsbuttonsmall"), for: .normal)
        hundredButton.imageView?.contentMode = .scaleAspectFit
        hundredButton.addTarget(self, action: #selector(CoinsStoreView.buyHundred), for: .touchUpInside)
        storeView.addSubview(hundredButton)
        
        hundredButtonLabel = UILabel(frame: CGRect(x: 5, y: 5, width: ((hundredButton.frame.size.width/5) * 3) + 5, height:hundredButton.frame.size.height/2))
        hundredButtonLabel.font = UIFont(name: "CurseCasualRegular", size: 250)
        hundredButtonLabel.baselineAdjustment = .alignCenters
        hundredButtonLabel.adjustsFontSizeToFitWidth = true
        hundredButtonLabel.textColor = UIColor(red: 0.08, green: 0.23, blue: 0.31, alpha: 1)
        hundredButtonLabel.textAlignment = .center
        hundredButtonLabel.text = "500 Coins"
        hundredButton.addSubview(hundredButtonLabel)
        
        hundredButtonPriceLabel = UILabel(frame: CGRect(x: hundredButtonLabel.frame.size.width / 4,
                                                        y: hundredButtonLabel.frame.origin.y + hundredButtonLabel.frame.size.height,
                                                        width: hundredButtonLabel.frame.size.width / 2,
                                                        height: hundredButton.frame.size.height/2 - 10))
        hundredButtonPriceLabel.font = UIFont(name: "CurseCasualRegular", size: 200)
        hundredButtonPriceLabel.baselineAdjustment = .alignCenters
        hundredButtonPriceLabel.adjustsFontSizeToFitWidth = true
        hundredButtonPriceLabel.textColor = UIColor(red: 0.08, green: 0.23, blue: 0.31, alpha: 1)
        hundredButtonPriceLabel.textAlignment = .center
        hundredButtonPriceLabel.text = ""
        hundredButton.addSubview(hundredButtonPriceLabel)
        
        
        thousandButton = UIButton(frame: CGRect(x: hundredButton.frame.origin.x,
                                                y: hundredButton.frame.origin.y + hundredButton.frame.size.height + storeView.frame.size.height * 0.05,
                                                width: hundredButton.frame.size.width,
                                                height: hundredButton.frame.size.height))
        thousandButton.isEnabled = false
        thousandButton.setImage(UIImage(named: "coinsbuttonmedium"), for: .normal)
        thousandButton.imageView?.contentMode = .scaleAspectFit
        thousandButton.addTarget(self, action: #selector(CoinsStoreView.buyThousand), for: .touchUpInside)
        storeView.addSubview(thousandButton)
        
        thousandButtonLabel = UILabel(frame: CGRect(x: 5, y: 5, width: ((thousandButton.frame.size.width/5) * 3) + 5, height:thousandButton.frame.size.height/2))
        thousandButtonLabel.font = UIFont(name: "CurseCasualRegular", size: 250)
        thousandButtonLabel.baselineAdjustment = .alignCenters
        thousandButtonLabel.adjustsFontSizeToFitWidth = true
        thousandButtonLabel.textColor = UIColor(red: 0.08, green: 0.23, blue: 0.31, alpha: 1)
        thousandButtonLabel.textAlignment = .center
        thousandButtonLabel.text = "2500 Coins"
        thousandButton.addSubview(thousandButtonLabel)
        
        thousandButtonPriceLabel = UILabel(frame: CGRect(x: thousandButtonLabel.frame.size.width / 4,
                                                        y: thousandButtonLabel.frame.origin.y + thousandButtonLabel.frame.size.height,
                                                        width: thousandButtonLabel.frame.size.width / 2,
                                                        height: thousandButton.frame.size.height/2 - 10))
        thousandButtonPriceLabel.font = UIFont(name: "CurseCasualRegular", size: 200)
        thousandButtonPriceLabel.baselineAdjustment = .alignCenters
        thousandButtonPriceLabel.adjustsFontSizeToFitWidth = true
        thousandButtonPriceLabel.textColor = UIColor(red: 0.08, green: 0.23, blue: 0.31, alpha: 1)
        thousandButtonPriceLabel.textAlignment = .center
        thousandButtonPriceLabel.text = ""
        thousandButton.addSubview(thousandButtonPriceLabel)

        
        
        tenThousandButton = UIButton(frame: CGRect(x: hundredButton.frame.origin.x,
                                                   y: thousandButton.frame.origin.y + thousandButton.frame.size.height + storeView.frame.size.height * 0.05,
                                                   width: hundredButton.frame.size.width,
                                                   height: hundredButton.frame.size.height))
        tenThousandButton.isEnabled = false
        tenThousandButton.setImage(UIImage(named: "coinsbuttonlarge"), for: .normal)
        tenThousandButton.imageView?.contentMode = .scaleAspectFit
        tenThousandButton.addTarget(self, action: #selector(CoinsStoreView.buyTenThousand), for: .touchUpInside)
        storeView.addSubview(tenThousandButton)
        
        tenThousandButtonLabel = UILabel(frame: CGRect(x: 5, y: 5, width: ((tenThousandButton.frame.size.width/5) * 3) + 5, height:tenThousandButton.frame.size.height/2))
        tenThousandButtonLabel.font = UIFont(name: "CurseCasualRegular", size: 250)
        tenThousandButtonLabel.baselineAdjustment = .alignCenters
        tenThousandButtonLabel.adjustsFontSizeToFitWidth = true
        tenThousandButtonLabel.textColor = UIColor(red: 0.08, green: 0.23, blue: 0.31, alpha: 1)
        tenThousandButtonLabel.textAlignment = .center
        tenThousandButtonLabel.text = "10000 Coins"
        tenThousandButton.addSubview(tenThousandButtonLabel)
        
        tenThousandButtonPriceLabel = UILabel(frame: CGRect(x: tenThousandButtonLabel.frame.size.width / 4,
                                                         y: tenThousandButtonLabel.frame.origin.y + tenThousandButtonLabel.frame.size.height,
                                                         width: tenThousandButtonLabel.frame.size.width / 2,
                                                         height: tenThousandButton.frame.size.height/2 - 10))
        tenThousandButtonPriceLabel.font = UIFont(name: "CurseCasualRegular", size: 200)
        tenThousandButtonPriceLabel.baselineAdjustment = .alignCenters
        tenThousandButtonPriceLabel.adjustsFontSizeToFitWidth = true
        tenThousandButtonPriceLabel.textColor = UIColor(red: 0.08, green: 0.23, blue: 0.31, alpha: 1)
        tenThousandButtonPriceLabel.textAlignment = .center
        tenThousandButtonPriceLabel.text = ""
        tenThousandButton.addSubview(tenThousandButtonPriceLabel)
        
        freeButton = UIButton(frame: CGRect(x: storeView.frame.size.width / 2 - (tenThousandButton.frame.size.width / 2),
                                                   y: tenThousandButton.frame.origin.y + tenThousandButton.frame.size.height + storeView.frame.size.height * 0.05,
                                                   width: tenThousandButton.frame.size.width,
                                                   height: tenThousandButton.frame.size.height))
        freeButton.backgroundColor = UIColor(red: 0.43, green: 0.72, blue: 0.87, alpha:1.0)
        freeButton.layer.cornerRadius = 6
        freeButton.isEnabled = true
        freeButton.setTitle("Watch Ad For 100 Free Coins", for: .normal)
        freeButton.titleLabel!.textAlignment = .center
        freeButton.setTitleColor(UIColor(red: 0.08, green: 0.23, blue: 0.31, alpha: 1), for: .normal)
        freeButton.titleLabel?.font = UIFont(name: "CurseCasualRegular", size: 180)
        freeButton.contentEdgeInsets = .init(top: 30, left: 20, bottom: 30, right: 20)
        freeButton.titleLabel!.adjustsFontSizeToFitWidth = true
        freeButton.titleLabel!.baselineAdjustment = .alignCenters
        freeButton.contentVerticalAlignment = .center
        freeButton.contentHorizontalAlignment = .center
        freeButton.titleLabel?.numberOfLines = 2
        freeButton.addTarget(self, action: #selector(CoinsStoreView.freeCoins), for: .touchUpInside)
        storeView.addSubview(freeButton)
        

    }

    required init?(coder aDecoder: NSCoder) {
        fatalError()
    }
    
    override func didMoveToSuperview() {

        UIView.animate(withDuration: 0.5,
                       delay: 0,
                       options: .curveEaseInOut,
                       animations:  {
                        self.frame.origin.x = 0
                        self.frame.origin.y = 0
                        /*self.storeView.frame.size.width = self.frame.size.width - 80
                        self.storeView.frame.size.height = self.frame.size.height - 160
                        self.storeView.frame.origin.x = 40
                        self.storeView.frame.origin.y = 80*/
        },
                       completion: { (completed) in
                        self.backButton.alpha = 1
                        self.backButton.isEnabled = true
                        //self.hundredButton.isEnabled = true
                        //self.thousandButton.isEnabled = true
                        //self.tenThousandButton.isEnabled = true
                        })
    }
    
    @objc func backButtonPressed()
    {
        //self.saveDataSource.saveChanges()
        SKTAudio.sharedInstance().playSoundEffect("buttonpress.wav")
        
        self.backButton.isEnabled = false
        self.hundredButton.isEnabled = false
        self.thousandButton.isEnabled = false
        self.tenThousandButton.isEnabled = false
        UIView.animate(withDuration: 0.5,
                       delay: 0.1,
                       options: .curveEaseOut,
                       animations: {
                        self.frame.origin.x -= self.superview!.frame.size.width
                            },
                       completion: {finished in
                        /*self.gameScene.hud.clothesShopNode.appear()
                        self.gameScene.gameState.enter(WaitingToStart.self)*/
                        self.gameScene.isPaused = false
                        self.gameScene.hud.coinsNode.isUserInteractionEnabled = true
                        self.gameScene.hud.clothesShopNode.appear()
                        self.removeFromSuperview()
        })
        
    }
    
    @objc func buyHundred()
    {
        if hundredProduct != nil
        {
            //hundredButton.isEnabled = false
            buyProduct(product: hundredProduct)
        }
        
        SKTAudio.sharedInstance().playSoundEffect("buttonpress.wav")
    }
    
    @objc func buyThousand()
    {
        if thousandProduct != nil
        {
            //thousandButton.isEnabled = false
            buyProduct(product: thousandProduct)
        }
        
        SKTAudio.sharedInstance().playSoundEffect("buttonpress.wav")
    }
    
    @objc func buyTenThousand()
    {
        if tenThousandProduct != nil
        {
            //tenThousandButton.isEnabled = false
            buyProduct(product: tenThousandProduct)
        }
        
        SKTAudio.sharedInstance().playSoundEffect("buttonpress.wav")
    }
    
    @objc func freeCoins()
    {
        
        if GADRewardBasedVideoAd.sharedInstance().isReady == true
        {
            let gvc = gameScene.view!.window!.rootViewController! as! GameViewController
            gvc.presentRewardedAd()
        }
    }
    
    func buyProduct(product: SKProduct){
        var payment = SKPayment(product: product)
        SKPaymentQueue.default().add(payment)
    }
}

extension CoinsStoreView: SKProductsRequestDelegate
{
    func productsRequest(_ request: SKProductsRequest, didReceive response: SKProductsResponse)
    {
        for product in response.products
        {
            switch product.productIdentifier
            {
            case hundredCoinsID:
                let price = Float.init(truncating: product.price)
                var string = String()
                if product.priceLocale.currencySymbol != nil {
                    string.append(product.priceLocale.currencySymbol!)
                }
                string.append(String(price))
                
                //hundredButton.setTitle("\(product.localizedTitle) \(string)", for: .normal)
                
                
                hundredButtonLabel.text = "500 Coins"
                hundredButtonPriceLabel.text = string
                hundredButton.isEnabled = true
                hundredProduct = product
                
            case twoFiveCoinsID:
                let price = Float.init(truncating: product.price)
                var string = String()
                if product.priceLocale.currencySymbol != nil {
                    string.append(product.priceLocale.currencySymbol!)
                }
                string.append(String(price))
                //thousandButton.setTitle("\(product.localizedTitle) \(string)", for: .normal)
                thousandButtonLabel.text = "2500 Coins"
                thousandButtonPriceLabel.text = string
                thousandButton.isEnabled = true
                thousandProduct = product
            case tenThousandCoinsID:
                let price = Float.init(truncating: product.price)
                var string = String()
                if product.priceLocale.currencySymbol != nil {
                    string.append(product.priceLocale.currencySymbol!)
                }
                string.append(String(price))
                print("iap", product.localizedTitle)
                tenThousandButtonLabel.text = "10000 Coins"
                tenThousandButtonPriceLabel.text = string
                tenThousandButton.isEnabled = true
                tenThousandProduct = product
            default:
                break
            }
        }
    }
    
  
}
extension CoinsStoreView: SKPaymentTransactionObserver
{
    func paymentQueue(_ queue: SKPaymentQueue, updatedTransactions transactions: [SKPaymentTransaction]) {
        
        for transaction in transactions
        {
                switch transaction.transactionState {
                case .purchased:
                    
                    switch transaction.payment.productIdentifier {
                    case hundredCoinsID:
                        gameScene.saveDataSource.coins += 500
                    case twoFiveCoinsID:
                        gameScene.saveDataSource.coins += 2500
                    case tenThousandCoinsID:
                        gameScene.saveDataSource.coins += 10000
                    default:
                        break
                    }
                    
                    gameScene.saveDataSource.saveChanges()
                    gameScene.hud.coinsNode.updateCoinsLabel()
                    SKTAudio.sharedInstance().playSoundEffect("coin.wav")
                    SKPaymentQueue.default().finishTransaction(transaction)
                    break;
                case .failed:
                    SKTAudio.sharedInstance().playSoundEffect("nounlock.wav")
                    SKPaymentQueue.default().finishTransaction(transaction)
                    break
                    // case .Restored:
                //[self restoreTransaction:transaction];
                default:
                    break
                }
        }
    }
}
