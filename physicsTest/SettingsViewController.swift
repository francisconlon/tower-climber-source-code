//
//  SettingsViewController.swift
//  physicsTest
//
//  Created by Francis Conlon on 22/02/2019.
//  Copyright © 2019 OvervoltStudios. All rights reserved.
//

import UIKit
import PersonalizedAdConsent

class SettingsViewController: UIViewController {

    @IBOutlet var creditsScrollView: UIScrollView!
    @IBOutlet var contentScrollView: UIScrollView!
    @IBOutlet var advertCheckMark: UIImageView!
    @IBOutlet var advertLabel: UILabel!
    @IBOutlet var musicCheckmark: UIImageView!
    
    var saveDataSource: SaveDataSource!
    
    override func viewDidLoad()
    {
        if SKTAudio.sharedInstance().backgroundMusicPlayer != nil && SKTAudio.sharedInstance().backgroundMusicPlayer!.isPlaying {
            musicCheckmark.image = UIImage(named: "checkmarkclosed.png")
        } else {
            musicCheckmark.image = UIImage(named: "checkmarkopen.png")
        }
        
        super.viewDidLoad()
        
        let credits = NSArray(contentsOfFile: Bundle.main.path(forResource: "CreditsPropertyList", ofType: ".plist")!)!
        var positionYCredits = 0
        let width = view.frame.size.width
        for credit in credits
        {
            let label = UILabel(frame: CGRect(x: 10, y: positionYCredits, width: Int(width), height: 20))
            label.font = UIFont(name: "CurseCasualRegular", size: 16)
            label.text = String("\(credit)")
            label.textColor = UIColor.white
            creditsScrollView.addSubview(label)
            positionYCredits += Int(label.frame.size.height)
        }
        creditsScrollView.contentSize = CGSize(width: Int(width * 1.5),
                                               height: positionYCredits)
        
        
        if PACConsentInformation.sharedInstance.isRequestLocationInEEAOrUnknown
        {
            let advertisers = PACConsentInformation.sharedInstance.adProviders!
            var positionY = 0
            let width = Int(view.frame.size.width)
            for advertiser in advertisers
            {
                
                let label = UILabel(frame: CGRect(x: 0, y: positionY, width: width * 2, height: 20))
                label.font = UIFont(name: "CurseCasualRegular", size: 16)
                label.text = String("\(advertiser.name) - \(advertiser.privacyPolicyURL)")
                label.textColor = UIColor.white
                contentScrollView.addSubview(label)
                positionY += Int(label.frame.size.height)
            }
            contentScrollView.contentSize = CGSize(width: width * 2,
                                                   height: positionY)
            
            if PACConsentInformation.sharedInstance.consentStatus == .personalized
            {
                advertCheckMark.image = UIImage(named: "checkmarkclosed.png")
            } else if PACConsentInformation.sharedInstance.consentStatus == .nonPersonalized {
                advertCheckMark.image = UIImage(named: "checkmarkopen.png")
            }
        } else {
            advertCheckMark.removeFromSuperview()
            advertLabel.removeFromSuperview()
            contentScrollView.removeFromSuperview()
        }
        // Do any additional setup after loading the view.
    }

    
    @IBAction func checkMarkTapped(_ sender: UITapGestureRecognizer)
    {
        if PACConsentInformation.sharedInstance.consentStatus == .personalized {
            PACConsentInformation.sharedInstance.consentStatus = .nonPersonalized
            advertCheckMark.image = UIImage(named: "checkmarkopen.png")
        } else if PACConsentInformation.sharedInstance.consentStatus == .nonPersonalized {
            PACConsentInformation.sharedInstance.consentStatus = .personalized
            advertCheckMark.image = UIImage(named: "checkmarkclosed.png")
        }
    }
    
    @IBAction func backgroundMusicTapped(_ sender: Any)
    {
        if saveDataSource.backgroundMusic {
            saveDataSource.backgroundMusic = false
            musicCheckmark.image = UIImage(named: "checkmarkopen.png")
            SKTAudio.sharedInstance().backgroundMusicPlayer?.stop()
        } else if saveDataSource.backgroundMusic == false {
            saveDataSource.backgroundMusic = true
            musicCheckmark.image = UIImage(named: "checkmarkclosed.png")
            SKTAudio.sharedInstance().playBackgroundMusic("Rhinoceros.mp3")
            SKTAudio.sharedInstance().backgroundMusicPlayer!.volume = 0.5
        }
    }
    
    @IBAction func backButtonPressed(_ sender: UIButton)
    {
        self.dismiss(animated: true, completion: nil)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */
    
    
}
